name := "leetcode"

version := "0.1"

scalaVersion := "2.13.8"

libraryDependencies ++= Seq(
  compilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
  compilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full),
  "org.typelevel" %% "cats-core" % "2.7.0",
  "org.typelevel" %% "cats-effect" % "3.3.11"
)