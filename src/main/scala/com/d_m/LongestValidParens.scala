package com.d_m

import cats.effect.IO

import scala.io.StdIn

object LongestValidParens {
  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy(expr)
  }

  def longestValidParentheses(s: String): Int = {
    def build(i: Int): Lazy[Int] = Lazy {
      if (i == 0) {
        0
      } else {
        // i = current index
        // table(i - 1) = number of valid parens at i - 1
        // i - table(i - 1) = index at the beginning of the valid parens
        lazy val startIdx = i - table(i - 1)()
        (s(i - 1), s(i)) match {
          case ('(', ')') => (if (i >= 2) table(i - 2)() else 0) + 2
          case (')', ')') if startIdx > 0 && s(startIdx - 1) == '(' =>
            // if character before valid parens was a '(' combine:
            // the longest valid parens before the invalid '(' | table(i - table(i - 1) - 2) +
            // the current longest valid parens                | table(i - 1) +
            // 2 for the newest ')' closing the '('.           | 2
            table(i - 1)() + (if (startIdx >= 2) table(startIdx - 2)() else 0) + 2
          case _ => 0
        }
      }
    }
    lazy val table = Array.tabulate(s.length)(build)
    if (s.isEmpty) 0 else table.map(_ ()).max
  }

  def longestValidParenthesesStack(s: String): Int =
    s.zipWithIndex.foldLeft((List(-1), 0)) {
      case ((stack, max), (ch, i)) => (ch, stack) match {
        case ('(', _) => (i :: stack, max)
        case (')', _ :: Nil) => (i :: Nil, max)
        case (')', _ :: start :: is) => (start :: is, max.max(i - start))
        case _ => throw new RuntimeException("This should not happen")
      }
    }._2

  def longestValidParenthesesNoSpace(s: String): Int = {
    def foldMaxValid(leftToRight: Boolean)
                    (tuple: (Int, Int, Int), ch: Char): (Int, Int, Int) = tuple match {
      case (max, oldLeft, oldRight) =>
        val (left, right) = ch match {
          case '(' => (oldLeft + 1, oldRight)
          case ')' => (oldLeft, oldRight + 1)
        }
        if ((leftToRight && right > left) || (!leftToRight && left > right))
          (max, 0, 0)
        else if (left == right)
          (max.max(2 * (if (leftToRight) right else left)), left, right)
        else
          (max, left, right)
    }
    val max = s.foldLeft((0, 0, 0))(foldMaxValid(true))._1
    s.foldRight((max, 0, 0))((a, b) => foldMaxValid(false)(b, a))._1
  }

  val approaches: Map[String, String => Int] = Map(
    "dynamic programming" -> longestValidParentheses,
    "stack"               -> longestValidParenthesesStack,
    "no space"            -> longestValidParenthesesNoSpace,
  )

  def run: IO[Unit] =
    for {
      fn <- IOUtils.promptApproach(approaches)
      _  <- IO(println("Enter the string to find longest valid parentheses in:"))
      s  <- IO(StdIn.readLine)
      _  <- IO(println("Longest valid parentheses length is: " + fn(s)))
    } yield ()
}
