package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object Atoi {
  def myAtoi(str: String): Int = {
    def takeOptionalSign(s: String): Option[(Char, String)] = s.headOption.flatMap {
      case '+' => Some('+', s.tail)
      case '-' => Some('-', s.tail)
      case c if c.isDigit => Some('+', s)
      case _ => None
    }
    def checkedCons(n: Int, d: Int): Int =
      if (n > Int.MaxValue / 10 || (n == Int.MaxValue / 10 && d > 7))
        Int.MaxValue
      else if (n < Int.MinValue / 10 || (n == Int.MinValue / 10 && d < -8))
        Int.MinValue
      else
        n * 10 + d

    val stripped = str.dropWhile(_ == ' ')
    takeOptionalSign(stripped).map { case (sign, rest) =>
      val digits = rest.takeWhile(_.isDigit).map(_.asDigit)
      val signedDigits = if (sign == '-') digits.map(-_) else digits
      signedDigits.foldLeft(0)(checkedCons)
    }.getOrElse(0)
  }

  def run: IO[Unit] =
    for {
      _   <- IO(println("Enter the string to convert to number:"))
      str <- IO(StdIn.readLine)
      _   <- IO(println("Result: " + myAtoi(str)))
    } yield ()
}
