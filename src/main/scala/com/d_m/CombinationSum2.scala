package com.d_m

import cats.effect.IO

import scala.io.StdIn

object CombinationSum2 {
  def combinationSum2(candidates: Array[Int], target: Int): List[List[Int]] = {
    def go(candidates: Map[Int, Int], build: Set[List[Int]], curr: List[Int]): Set[List[Int]] = {
      val sum = curr.sum
      if (sum > target) {
        build
      } else if (sum == target) {
        build + curr.sorted
      } else {
        candidates.filter(_._2 > 0).foldLeft(build) {
          case (build, (k, v)) =>
            val updatedCandidates = candidates + (k -> (v - 1))
            go(updatedCandidates, build, k :: curr)
        }
      }
    }
    def buildMap(candidates: Array[Int]): Map[Int, Int] =
      candidates.foldLeft(Map[Int, Int]()) { (map, e) =>
        map.updatedWith(e)(v => Some(v.map(_ + 1).getOrElse(1)))
      }
    go(buildMap(candidates), Set(), List()).toList
  }

  def run: IO[Unit] =
    for {
      _          <- IO(println("Enter an array of numbers separated by spaces:"))
      candidates <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt).toArray)
      _          <- IO(println("Enter the target:"))
      target     <- IO(StdIn.readInt)
      result      = combinationSum2(candidates, target)
      _          <- IO(println("Result is: [" +
        result.map("[" + _.mkString(",") + "]").mkString(",") + "]"))
    } yield ()
}
