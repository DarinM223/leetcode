package com.d_m

import cats.effect.IO

import scala.io.StdIn

object IntToRoman {
  def intToRoman(num: Int): String = {
    val numbers = List(
      1000 -> "M",
      900  -> "CM",
      500  -> "D",
      400  -> "CD",
      100  -> "C",
      90   -> "XC",
      50   -> "L",
      40   -> "XL",
      10   -> "X",
      9    -> "IX",
      5    -> "V",
      4    -> "IV",
      1    -> "I"
    )
    def go(num: Int, numbers: List[(Int, String)]): String = numbers match {
      case (value, s) :: _ if num / value > 0 => s + go(num - value, numbers)
      case _ :: rest => go(num, rest)
      case _ => ""
    }
    go(num, numbers)
  }

  def run: IO[Unit] =
    for {
      _   <- IO(println("Enter the number to convert to roman numerals:"))
      num <- IO(StdIn.readInt)
      _   <- IO(println("Result is: " + intToRoman(num)))
    } yield ()
}
