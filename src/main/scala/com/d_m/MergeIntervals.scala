package com.d_m

import cats.effect.IO

import scala.io.StdIn

object MergeIntervals {
  // 1. Build a graph where every edge between intervals runs in both directions.
  // 2. For each node, figure out which "component" each node is in. A component
  // is a group of nodes that are connected to each other. Each component
  // has its own integer id and the all the nodes in a component are marked that ID
  // using DFS graph traversal.
  // 3. Merge all interval nodes with the same component ID into one big interval.
  //
  // This is a rediculous amount of code to solve the problem and runs out of space
  // in Leetcode tests.
  def merge(intervals: Array[Array[Int]]): Array[Array[Int]] = {
    type ComponentID = Int
    type Interval = Array[Int]

    def overlap(i1: Interval, i2: Interval): Boolean =
      i1(0) <= i2(1) && i2(0) <= i1(1)
    def markDFS(
      i: Interval,
      componentID: ComponentID,
      componentNodes: Map[ComponentID, List[Interval]],
      graph: Map[Interval, List[Interval]],
      visited: Set[Interval]
    ): (Map[ComponentID, List[Interval]], Set[Interval]) =
      if (visited.contains(i)) {
        (componentNodes, visited)
      } else {
        val componentNodes2 = componentNodes +
          (componentID -> (i :: componentNodes.getOrElse(componentID, List())))
        graph.getOrElse(i, List()).foldLeft((componentNodes2, visited + i)) {
          case ((componentNodes, visited), child) =>
            markDFS(child, componentID, componentNodes, graph, visited)
        }
      }
    def mergeNodes(nodes: List[Interval]): Interval =
      Array(nodes.map(_(0)).min, nodes.map(_(1)).max)

    val initGraph: Map[Interval, List[Interval]] =
      intervals.map((_, List())).toMap
    val intervalPairs =
      for {
        i1 <- intervals
        i2 <- intervals
      } yield (i1, i2)
    val graph = intervalPairs.foldLeft(initGraph) {
      case (graph, (i1, i2)) =>
        if (overlap(i1, i2))
          graph + (i1 -> (i2 :: graph(i1))) + (i2 -> (i1 :: graph(i2)))
        else
          graph
    }
    val componentNodes = intervals.foldLeft((Map[ComponentID, List[Interval]](), Set[Interval](), 0)) {
      case ((componentNodes, visited, componentID), i) =>
        val (componentNodes2, visited2) = markDFS(i, componentID, componentNodes, graph, visited)
        (componentNodes2, visited2, componentID + 1)
    }._1
    componentNodes.foldLeft(List[Interval]()) {
      case (merged, (_, nodes)) => mergeNodes(nodes) :: merged
    }.toArray
  }

  // First sort the intervals by interval start.
  // Then for every interval, grow a list of results by either
  // appending to the list or extending the previous interval in the list.
  def mergeSorted(intervals: Array[Array[Int]]): Array[Array[Int]] =
    intervals.sortBy(_(0)).foldLeft(List[Array[Int]]()) {
      (merged, interval) => merged match {
        case Nil => interval :: merged
        // If interval doesn't overlap with previous
        case head :: _ if head(1) < interval(0) => interval :: merged
        // Merge the current interval and previous interval if there is overlap.
        case Array(start, end) :: tail => Array(start, end.max(interval(1))) :: tail
      }
    }.toArray

  def run: IO[Unit] =
    for {
      _         <- IO(println("Enter an array of intervals:"))
      _         <- IO(println("Example: 1,4 4,5"))
      intervals <- IO(StdIn.readLine).map(_.split(' ').map(_.split(',').map(_.toInt)))
      _         <- IO(println("Merged intervals: " +
        mergeSorted(intervals).map(_.mkString(",")).mkString(" ")))
    } yield ()
}