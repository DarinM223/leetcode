package com.d_m
import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object ClimbingStairs {
  def climbStairsBruteForce(n: Int): Int = {
    def go(step: Int): Int =
      if (step > n)
        0
      else if (step == n)
        1
      else
        go(step + 1) + go(step + 2)
    go(0)
  }

  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy[T](expr)
  }

  def climbStairsDP(n: Int): Int = {
    def build(step: Int): Lazy[Int] = Lazy {
      if (step > n)
        0
      else if (step == n)
        1
      else
        table(step + 1)() + table(step + 2)()
    }
    lazy val table = Array.tabulate(n + 2)(build)
    table(0)()
  }

  // nth number of the fibonacci series where
  // Fib(1) = 1 and Fib(2) = 2
  def climbStairsFiboSeries(n: Int): Int =
    if (n == 1) {
      1
    } else {
      (3 to n).foldLeft((1, 2)) {
        case ((first, second), _) =>
          val third = first + second
          (second, third)
      }._2
    }

  // nth number of fibonacci series can also be
  // Q^n [0, 0] where Q is a matrix {{1, 1}, {1, 0}}
  def climbStairsBinets(n: Int): Int = {
    def mult(a: Array[Array[Int]], b: Array[Array[Int]]): Array[Array[Int]] = {
      def build(i: Int, j: Int): Int = a(i)(0) * b(0)(j) + a(i)(1) * b(1)(j)
      Array.tabulate(2, 2)(build)
    }
    def pow(a: Array[Array[Int]], n: Int): Array[Array[Int]] = {
      @tailrec def go(ret: Array[Array[Int]], n: Int, a: Array[Array[Int]]): Array[Array[Int]] =
        if (n <= 0)
          ret
        else
          go(if ((n & 1) == 1) mult(ret, a) else ret, n >> 1, mult(a, a))
      go(Array(Array(1, 0), Array(0, 1)), n, a)
    }
    val q = Array(Array(1, 1), Array(1, 0))
    pow(q, n)(0)(0)
  }

  // There is also a formula for getting the nth number in the fibonacci series.
  // This formula may not work for very large values of n.
  def climbStairsFiboFormula(n: Int): Int = {
    import scala.math._
    val sqrt5 = sqrt(5)
    val fibn = pow((1 + sqrt5) / 2, n + 1) - pow((1 - sqrt5) / 2, n + 1)
    (fibn / sqrt5).toInt
  }

  def approaches: Map[String, Int => Int] = Map(
    "brute force" -> climbStairsBruteForce,
    "dynamic programming" -> climbStairsDP,
    "fibonacci series"    -> climbStairsFiboSeries,
    "binets"              -> climbStairsBinets,
    "fibonacci formula"   -> climbStairsFiboFormula,
  )

  def run: IO[Unit] =
    for {
      f <- IOUtils.promptApproach(approaches)
      _ <- IO(println("Enter the number of steps:"))
      n <- IO(StdIn.readInt)
      _ <- IO(println("There are " + f(n) + " ways to climb to the top"))
    } yield ()
}