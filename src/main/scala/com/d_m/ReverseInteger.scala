package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object ReverseInteger {
  // Treat integers like linked lists of digits
  // 123 => 1 <- 2 <- 3
  // 3 is the head, 1 <- 2 is the tail
  // Can pattern match head of list with (l % 10, l / 10)
  // Can cons like (e:l) with (l * 10 + e)
  //
  // Solution is similar to foldl (flip (:)) [] but manually using recursion.
  def reverse(x: Int): Int = {
    // Int.MaxValue - (Int.MaxValue / 10 * 10) = 7
    // Int.MinValue - (Int.MinValue / 10 * 10) = -8
    def cons(xs: Int, x: Int): Option[Int] =
      if ((xs > Int.MaxValue / 10) ||
          (xs == Int.MaxValue / 10 && x > 7) ||
          (xs < Int.MinValue / 10) ||
          (xs == Int.MinValue / 10 && x < -8))
        None
      else
        Some(xs * 10 + x)

    @tailrec def go(acc: Int, n: Int): Int = {
      val (x, xs) = (n % 10, n / 10)
      if (n == 0) {
        acc
      } else {
        cons(acc, x) match {
          case Some(updatedAcc) => go(updatedAcc, xs)
          case None => 0
        }
      }
    }
    go(0, x)
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter integer to reverse:"))
      x <- IO(StdIn.readInt)
      _ <- IO(println("Reversed integer: " + reverse(x)))
    } yield ()
}
