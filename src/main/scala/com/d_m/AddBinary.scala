package com.d_m
import cats.effect.IO
import scala.io.StdIn

object AddBinary {
  def addBinary(a: String, b: String): String = {
    def go(a: List[Char], b: List[Char]): (List[Char], Int) = (a, b) match {
      case (h1 :: t1, h2 :: t2) =>
        val (result, carry) = go(t1, t2)
        val (digit, newCarry) = ((h1 - '0') + (h2 - '0') + carry) match {
          case 3 => ('1', 1)
          case 2 => ('0', 1)
          case 1 => ('1', 0)
          case _ => ('0', 0)
        }
        (digit :: result, newCarry)
      case (_, _) => (Nil, 0)
    }
    val (a2: List[Char], b2: List[Char]) =
      if (a.length > b.length)
        (a.toList, List.fill(a.length - b.length)('0') ++ b.toList)
      else
        (List.fill(b.length - a.length)('0') ++ a.toList, b.toList)
    val (result, carry) = go(a2, b2)
    if (carry > 0)
      (('0' + carry).toChar :: result).mkString
    else
      result.mkString
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter the first binary number:"))
      a <- IO(StdIn.readLine).map(_.filter(c => c == '0' || c == '1'))
      _ <- IO(println("Enter the second binary number:"))
      b <- IO(StdIn.readLine).map(_.filter(c => c == '0' || c == '1'))
      _ <- IO(println(a + " + " + b + " is: " + addBinary(a, b)))
    } yield ()
}