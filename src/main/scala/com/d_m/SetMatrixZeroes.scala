package com.d_m
import cats.effect.IO

object SetMatrixZeroes {
  // There is a similar CTCI solution to this problem.
  // The first row and column store whether there was a 0 in that row or column.
  // The (0, 0) index is for checking if the first row had a zero from the start.
  // Use another variable to check if the first column had a zero from the start.
  def setZeroes(matrix: Array[Array[Int]]): Unit = {
    // NOTE: In-place means O(1) space. One extra variable is still O(1) space.
    var firstColMarked = false

    for (row <- matrix.indices) {
      if (matrix(row)(0) == 0) firstColMarked = true
      for (col <- 1 until matrix(0).length) {
        if (matrix(row)(col) == 0) {
          matrix(row)(0) = 0
          matrix(0)(col) = 0
        }
      }
    }

    for (row <- 1 until matrix.length) {
      for (col <- 1 until matrix(0).length) {
        if (matrix(row)(0) == 0 || matrix(0)(col) == 0) {
          matrix(row)(col) = 0
        }
      }
    }

    // When first row had a 0 in it, mark the whole first row with 0s.
    if (matrix(0)(0) == 0) {
      matrix(0).indices.foreach(matrix(0)(_) = 0)
    }
    if (firstColMarked) {
      matrix.indices.foreach(matrix(_)(0) = 0)
    }
  }

  def run: IO[Unit] = IOUtils.promptGrid.flatMap { matrix =>
    IO {
      setZeroes(matrix)
      println("Matrix with zeros set:")
      println(matrix.map(_.mkString(" ")).mkString("\n"))
    }
  }
}