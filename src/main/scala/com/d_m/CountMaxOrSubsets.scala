package com.d_m

object CountMaxOrSubsets {
  def countMaxOrSubsets(nums: Array[Int]): Int = {
    val max = nums.tail.foldLeft(nums.head)((acc, n) => acc | n)
    def go(subset: Int, i: Int): Int =
      if (i >= nums.length)
        if (subset == max) 1 else 0
      else
        go(subset, i + 1) + go(subset | nums(i), i + 1)
    go(0, 0)
  }
}
