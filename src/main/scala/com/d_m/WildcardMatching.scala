package com.d_m

import cats.effect.IO

import scala.io.StdIn

object WildcardMatching {
  def isMatchBruteForce(s: String, p: String): Boolean = {
    def go(i: Int, j: Int): Boolean =
      if (i >= s.length && j >= p.length) {
        true
      } else if (i >= s.length) {
        if (p(j) == '*') go(i, j + 1) else false
      } else if (j >= p.length) {
        false
      } else {
        (s(i), p(j)) match {
          case (c1, c2) if c1 == c2 => go(i + 1, j + 1)
          case (_, '?') => go(i + 1, j + 1)
          case (_, '*') => go(i, j + 1) || go(i + 1, j + 1) || go(i + 1, j)
          case _ => false
        }
      }
    go(0, 0)
  }

  class Lazy[T](expr: => T) {
    lazy val value = expr

    def apply(): T = this.value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy[T](expr)
  }

  def isMatchDP(s: String, p: String): Boolean = {
    def build(i: Int, j: Int): Lazy[Boolean] = Lazy {
      if (i >= s.length && j >= p.length) {
        true
      } else if (i >= s.length) {
        if (p(j) == '*') table(i)(j + 1)() else false
      } else if (j >= p.length) {
        false
      } else {
        (s(i), p(j)) match {
          case (c1, c2) if c1 == c2 => table(i + 1)(j + 1)()
          case (_, '?') => table(i + 1)(j + 1)()
          case (_, '*') => table(i)(j + 1)() || table(i + 1)(j + 1)() || table(i + 1)(j)()
          case _ => false
        }
      }
    }
    lazy val table = Array.tabulate(s.length + 1, p.length + 1)(build)
    table(0)(0)()
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter the input string that is either empty or contains lower case letters a-z:"))
      s <- IO(StdIn.readLine).map(_.filter(('a' to 'z').contains(_)))
      _ <- IO(println("Input string is: \"" + s + "\""))
      _ <- IO(println("Enter the pattern that is either empty or contains a-z, ?, or *"))
      p <- IO(StdIn.readLine).map(_.filter(c => ('a' to 'z').contains(c) || c == '?' || c == '*'))
      _ <- IO(println("Pattern is: \"" + p + "\""))
      _ <- IO(println("Match result is: " + isMatchDP(s, p)))
    } yield ()
}
