package com.d_m

import cats.effect.IO

import scala.io.StdIn

object MergeSortedLists {
  def mergeTwoLists(l1: ListNode, l2: ListNode): ListNode = {
    import ListNode._
    fromList(mergeTwoLists(toList(l1), toList(l2)))
  }

  def mergeTwoLists(l1: List[Int], l2: List[Int]): List[Int] = (l1, l2) match {
    case (h1 :: r1, h2 :: r2) if h1 < h2 => h1 :: mergeTwoLists(r1, h2 :: r2)
    case (h1 :: r1, h2 :: r2) => h2 :: mergeTwoLists(h1 :: r1, r2)
    case (l, Nil) => l
    case (Nil, l) => l
  }

  def run: IO[Unit] =
    for {
      _  <- IO(println("Enter first list of numbers (separated by spaces):"))
      l1 <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt).toList)
      _  <- IO(println("Enter second list of numbers (separated by spaces):"))
      l2 <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt).toList)
      _  <- IO(println("Merged list is: " + mergeTwoLists(l1, l2).mkString(" ")))
    } yield ()
}
