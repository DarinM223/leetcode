package com.d_m

import cats.effect.IO

import scala.io.StdIn

object Permutations {
  def permute(nums: Array[Int]): List[List[Int]] = {
    def go(build: List[List[Int]], curr: List[Int], used: Set[Int]): List[List[Int]] =
      if (used.size == nums.length) {
        curr :: build
      } else {
        nums
          .filter(!used.contains(_))
          .foldLeft(build)((build, num) => go(build, num :: curr, used + num))
      }
    go(List(), List(), Set())
  }

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter an array of distinct integers separated by spaces:"))
      nums <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _    <- IO(println("Permutations: [" +
        permute(nums).map("[" + _.mkString(",") + "]").mkString(",") + "]"))
    } yield ()
}
