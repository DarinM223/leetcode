package com.d_m

import cats.effect.IO

import scala.io.StdIn

object FourSum {
  def fourSum(nums: Array[Int], target: Int): List[List[Int]] = {
    val map = nums.zipWithIndex.toMap
    val indexes =
      for {
        i <- nums.indices
        j <- i + 1 until nums.length
        k <- j + 1 until nums.length
      } yield (i, j, k)
    indexes.foldLeft(Set[List[Int]]()) {
      case (s, (i, j, k)) =>
        val key = target - nums(i) - nums(j) - nums(k)
        map.get(key) match {
          case Some(x) if x > k =>
            s + List(nums(i), nums(j), nums(k), key).sorted
          case _ => s
        }
    }.toList
  }

  def run: IO[Unit] =
    for {
      _      <- IO(println("Enter an array of numbers separated by spaces:"))
      nums   <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _      <- IO(println("Enter the target:"))
      target <- IO(StdIn.readInt)
      _      <- IO(println("Result: " + fourSum(nums, target)))
    } yield ()
}
