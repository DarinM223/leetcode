package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object NextPermutation {
  // 1. Starting from the right, find the first number that is smaller than the
  // number to the right of it. The index of this number is i.
  // 2. Starting from the right, find the first number that is greater than the number
  // at i. The index of this number is j.
  // 3. Swap the numbers at i and j.
  // 4. Reverse the right side of the array starting from i + 1.
  //
  // Sadly leetcode doesn't allow cats/scalaz IO and prevents using
  // immutability for this problem so we have to break referential transparency here.
  def nextPermutation(nums: Array[Int]): Unit = {
    @tailrec def findFirstDecreasing(i: Int): Int =
      if (i >= 0 && nums(i + 1) <= nums(i)) findFirstDecreasing(i - 1) else i
    @tailrec def findFirstGreater(i: Int, j: Int): Int =
      if (j >= 0 && nums(j) <= nums(i)) findFirstGreater(i, j - 1) else j
    val i = findFirstDecreasing(nums.length - 2)
    if (i >= 0) {
      val j = findFirstGreater(i, nums.length - 1)
      swap(nums, i, j)
    }
    reverse(nums, i + 1, nums.length - 1)
  }

  def swap(arr: Array[Int], i: Int, j: Int): Unit = {
    val temp = arr(i)
    arr(i) = arr(j)
    arr(j) = temp
  }

  def reverse(arr: Array[Int], start: Int, end: Int): Unit = {
    var (i, j) = (start, end)
    while (i < j) {
      swap(arr, i, j)
      i += 1
      j -= 1
    }
  }

  def run: IO[Unit] = IO {
    println("Enter array of integers separated by spaces:")
    val nums = StdIn.readLine.split(' ').map(_.toInt)
    nextPermutation(nums)
    println("Next permutation: " + nums.mkString(" "))
  }
}
