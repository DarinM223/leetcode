package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object Sqrt {
  def mySqrt(x: Int): Int =
    (0 to x).takeWhile(n => scala.math.pow(n, 2) <= x).lastOption.getOrElse(0)

  def mySqrtBinarySearch(x: Int): Int = {
    @tailrec def go(start: Int, end: Int, best: Int): Int =
      if (end < start) {
        best
      } else {
        val mid = (start + end) / 2
        val squared = scala.math.pow(mid, 2)
        if (squared == x)
          mid
        else if (squared > x)
          go(start, mid - 1, best)
        else
          go(mid + 1, end, mid)
      }
    go(0, x, 0)
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter the number:"))
      n <- IO(StdIn.readInt)
      _ <- IO(println("sqrt(" + n + ") = " + mySqrtBinarySearch(n)))
    } yield ()
}