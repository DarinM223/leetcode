package com.d_m

import cats.effect.IO

import scala.io.StdIn

object Pow {
  // Recursively call myPow for n / 2.
  // If n is odd, add an extra n if n is positive, and
  // If n is negative and odd, then divide by an extra n (n ^ -1 = 1 / n)
  def myPow(x: Double, n: Int): Double =
    if (n == 0) {
      1
    } else {
      val halfPow = myPow(x, n / 2)
      if (n % 2 == 0)
        halfPow * halfPow
      else if (n > 0)
        x * halfPow * halfPow
      else
        (halfPow * halfPow) / x
    }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter a double number:"))
      x <- IO(StdIn.readDouble)
      _ <- IO(println("Enter an integer power:"))
      n <- IO(StdIn.readInt)
      _ <- IO(println(x + "^" + n + " = " + myPow(x, n)))
    } yield ()
}
