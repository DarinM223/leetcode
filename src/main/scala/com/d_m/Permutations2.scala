package com.d_m

import cats.effect.IO

import scala.io.StdIn

object Permutations2 {
  def permuteUnique(nums: Array[Int]): List[List[Int]] = {
    def go(build: List[List[Int]], curr: List[Int], used: Map[Int, Int]): List[List[Int]] =
      if (used.values.sum == 0) {
        curr :: build
      } else {
        used.keys.filter(used.getOrElse(_, 0) > 0).foldLeft(build) { (build, num) =>
          go(build, num :: curr, used + (num -> (used(num) - 1)))
        }
      }
    val used = nums.groupBy(identity).map { case (k, v) => (k, v.length) }
    go(List(), List(), used)
  }

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter an array of integers separated by spaces:"))
      nums <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _    <- IO(println("Permutations: [" +
        permuteUnique(nums).map("[" + _.mkString(",") + "]").mkString(",") + "]"))
    } yield ()
}
