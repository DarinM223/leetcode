package com.d_m

import cats.effect.IO

import scala.io.StdIn

object GroupAnagrams {
  def groupAnagrams(strs: Array[String]): List[List[String]] =
    strs.toList.groupBy(_.sorted).values.toList

  // Categorize string by character count
  // So "aab" will be converted into "#2#1#0#0..." with "#0" for the rest
  // of the letters in the alphabet.
  def groupAnagramsCharCount(strs: Array[String]): List[List[String]] = {
    def charCount(str: String): String = {
      val map = str.groupBy(identity).map { case (k, v) => (k, v.length) }
      ('a' to 'z').foldLeft(new StringBuilder()) { (s, ch) =>
        s ++= map.get(ch).map("#" + _).getOrElse("#0")
      }.toString
    }
    strs.toList.groupBy(charCount).values.toList
  }

  val approaches: Map[String, Array[String] => List[List[String]]] = Map(
    "sorted"          -> groupAnagrams,
    "character count" -> groupAnagramsCharCount,
  )

  def run: IO[Unit] =
    for {
      fn   <- IOUtils.promptApproach(approaches)
      _    <- IO(println("Enter an array of strings of lowercase letters separated by spaces:"))
      strs <- IO(StdIn.readLine).map(_.split(' ').map(_.filter(_.isLetter).map(_.toLower)))
      _    <- IO(println("Array of strings is: " + strs.mkString(" ")))
      _    <- IO(println("Result is: [" +
        fn(strs).map("[" + _.mkString(",") + "]").mkString(",")))
    } yield ()
}
