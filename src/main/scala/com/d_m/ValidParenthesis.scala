package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object ValidParenthesis {
  def isValid(s: String): Boolean = {
    @tailrec def go(s: String, stack: List[Char]): Boolean = (stack, s.headOption) match {
      case ('{' :: cs, Some('}')) => go(s.tail, cs)
      case ('(' :: cs, Some(')')) => go(s.tail, cs)
      case ('[' :: cs, Some(']')) => go(s.tail, cs)
      case (_, Some('{')) => go(s.tail, '{' :: stack)
      case (_, Some('(')) => go(s.tail, '(' :: stack)
      case (_, Some('[')) => go(s.tail, '[' :: stack)
      case (Nil, None) => true
      case _ => false
    }
    go(s, List())
  }

  val validParens = List('(', ')', '{', '}', '[', ']')

  def run: IO[Unit] =
    for {
      _   <- IO(println("Enter a string with only characters '(',')','{','}','[', and ']':"))
      s   <- IO(StdIn.readLine).map(_.filter(validParens.contains(_)))
      text = if (isValid(s)) "valid" else "not valid"
      _   <- IO(println("String \"" + s + "\" is " + text))
    } yield ()
}
