package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object NQueens {
  def solveNQueens(n: Int): List[List[String]] = {
    def check(board: List[String], i: Int): Boolean = {
      def checkCol(board: List[String]): Boolean = board.forall(_(i) != 'Q')
      @tailrec def checkDiagonals(board: List[String], diff: Int): Boolean = board match {
        case row :: rest =>
          val (leftIndex, rightIndex) = (i - diff, i + diff)
          if ((row.indices.contains(leftIndex) && row(leftIndex) == 'Q') ||
              (row.indices.contains(rightIndex) && row(rightIndex) == 'Q'))
            false
          else
            checkDiagonals(rest, diff + 1)
        case Nil => true
      }
      checkCol(board) && checkDiagonals(board, 1)
    }
    // Permutations problem, place a queen in every column that's legal.
    def go(build: List[List[String]], curr: List[String]): List[List[String]] =
      if (curr.length == n) {
        curr :: build
      } else {
        val choices = (0 until n)
          .filter(check(curr, _))
          .map(i => (0 until i).map(_ => '.').mkString + "Q" + (i + 1 until n).map(_ => '.').mkString)
        choices.foldLeft(build)((build, choice) => go(build, choice :: curr))
      }
    go(List(), List())
  }

  def boardsToString(result: List[List[String]]): String =
    result.map(_.mkString("\n")).mkString("\n\n")

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter an integer n for placing n queens on a nxn chessboard:"))
      n <- IO(StdIn.readInt)
      _ <- IO(println("Solutions: \n" + boardsToString(solveNQueens(n))))
    } yield ()
}
