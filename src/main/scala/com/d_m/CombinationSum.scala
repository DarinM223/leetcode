package com.d_m

import cats.effect.IO

import scala.io.StdIn

object CombinationSum {
  // General solution similar to finding all permutations problem.
  def combinationSum(candidates: Array[Int], target: Int): List[List[Int]] = {
    def go(build: Set[List[Int]], curr: List[Int]): Set[List[Int]] = {
      val sum = curr.sum
      // sum > target works because you cannot have duplicate solutions
      // so if you "overshoot" the target and then go back with a negative number,
      // you could just do the negative number first so it won't overshoot.
      if (sum > target)
        build
      else if (sum == target)
        build + curr.sorted
      else
        candidates.foldLeft(build)((build, e) => go(build, e :: curr))
    }
    go(Set(), List()).toList
  }

  def run: IO[Unit] =
    for {
      _          <- IO(println("Enter a set of numbers (without duplicates) separated by spaces:"))
      candidates <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt).toSet.toArray)
      _          <- IO(println("The set is: " + candidates.mkString(" ")))
      _          <- IO(println("Enter the target:"))
      target     <- IO(StdIn.readInt)
      result      = combinationSum(candidates, target)
      _          <- IO(println("Result is: [" +
        result.map("[" + _.mkString(",") + "]").mkString(",") + "]"))
    } yield ()
}
