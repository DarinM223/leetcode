package com.d_m
import cats.effect.IO

object UniquePaths2 {
  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = this.value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy[T](expr)
  }

  def uniquePathsWithObstacles(obstacleGrid: Array[Array[Int]]): Int = {
    def build(row: Int, col: Int): Lazy[Int] = Lazy {
      if (row >= obstacleGrid.length ||
          col >= obstacleGrid(row).length ||
          obstacleGrid(row)(col) == 1)
        0
      else if (row == obstacleGrid.length - 1 && col == obstacleGrid(row).length - 1)
        1
      else
        table(row + 1)(col)() + table(row)(col + 1)()
    }
    lazy val table = Array.tabulate(obstacleGrid.length + 1, obstacleGrid(0).length + 1)(build)
    table(0)(0)()
  }

  def run: IO[Unit] =
    for {
      obstacleGrid <- IOUtils.promptGrid
      _            <- IO(println("# of unique paths: " + uniquePathsWithObstacles(obstacleGrid)))
    } yield ()
}