package com.d_m

import cats.effect.IO
import cats.implicits._

import scala.io.StdIn

object ZigZag {
  // Example:
  // 00000000001111
  // 01234567890123
  // PAYPALISHIRING
  // num rows: 4
  //
  // 0     6       12
  // 1   5 7    11 13
  // 2 4   8 10    14
  // 3     9       15
  // Step: 4 * 2 - 2 = 6
  // i values: 0, 6, 12
  // if row is 2, then i + row is 2, 8, 14
  // and i + step - row is 4, 10
  //
  // So i + row is the normal values in the row, and i + step - row
  // are the extra values created by the zigzag.
  def convert(s: String, numRows: Int): String =
    if (numRows == 1)
      s
    else
      (0 until numRows).map(rowString(s, numRows, _)).mkString

  def rowString(s: String, numRows: Int, row: Int): String = {
    val step = numRows + numRows - 2
    def buildRow(i: Int): List[Char] =
      if (i + row >= s.length) {
        Nil
      } else if (row != 0 &&
        row != numRows - 1 &&
        i + step - row < s.length) {
        s(i + row) :: s(i + step - row) :: buildRow(i + step)
      } else {
        s(i + row) :: buildRow(i + step)
      }
    buildRow(0).mkString
  }

  def printZigZag(s: String, numRows: Int): IO[Unit] = {
    def printSpaces(spaces: Int): IO[Unit] =
      (0 until spaces).toStream.traverse_(_ => IO(print(' ')))

    def printRow(row: Int, rowStr: String): IO[Unit] = {
      def traverseString(i: Int): IO[Unit] = {
        val stepLength = numRows + numRows - 1
        if (i >= rowStr.length) {
          IO.pure(())
        } else if (i == 0) {
          IO(print(rowStr(i))) >> traverseString(i + 1)
        } else if (row == 0 || row == numRows - 1) {
          printSpaces(stepLength - 2) >>
            IO(print(rowStr(i))) >>
            traverseString(i + 1)
        } else {
          val spacesAfter = 1 + 2 * (row - 1)
          val spacesBefore = stepLength - spacesAfter - 3
          val firstPart = printSpaces(spacesBefore) >>
            IO(print(rowStr(i))) >>
            printSpaces(spacesAfter)
          val secondPart =
            if (i + 1 < rowStr.length)
              firstPart >> IO(print(rowStr(i + 1))) >> traverseString(i + 2)
            else
              firstPart >> traverseString(i + 1)
          secondPart
        }
      }
      traverseString(0) >> IO(print('\n'))
    }

    if (numRows == 1)
      IO(println(s.mkString(" ")))
    else
      (0 until numRows).toList
        .traverse_(row => printRow(row, rowString(s, numRows, row)))
  }

  def run: IO[Unit] =
    for {
      _       <- IO(println("Input the string to zigzag:"))
      s       <- IO(StdIn.readLine)
      _       <- IO(println("Input the number of rows in the zigzag:"))
      numRows <- IO(StdIn.readInt)
      _       <- printZigZag(s, numRows)
      _       <- IO(println("Result: " + convert(s, numRows)))
    } yield ()
}
