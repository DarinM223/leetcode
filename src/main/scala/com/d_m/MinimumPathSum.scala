package com.d_m
import cats.effect.IO

object MinimumPathSum {
  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = this.value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy[T](expr)
  }

  def minPathSum(grid: Array[Array[Int]]): Int = {
    def build(row: Int, col: Int): Lazy[Int] = Lazy {
      val prevResults =
        List[(Int, Int)]((row - 1, col), (row, col - 1))
          .filter { case (row, col) => row >= 0 && col >= 0 }
          .map { case (row, col) => table(row)(col)() }
      grid(row)(col) + (if (prevResults.isEmpty) 0 else prevResults.min)
    }
    lazy val table = Array.tabulate(grid.length + 1, grid(0).length + 1)(build)
    table(grid.length - 1)(grid(0).length - 1)()
  }

  def run: IO[Unit] =
    for {
      grid <- IOUtils.promptGrid
      _    <- IO(println("Minimum path sum: " + minPathSum(grid)))
    } yield ()
}
