package com.d_m

import cats.effect.IO

import scala.io.StdIn

object LengthOfLastWord {
  def lengthOfLastWord(s: String): Int =
    s.trim().foldRight((false, 0)) {
      case (' ', (false, count)) => (true, count)
      case (_, (false, count)) => (false, count + 1)
      case (_, (true, count)) => (true, count)
    }._2

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter a string:"))
      s <- IO(StdIn.readLine)
      _ <- IO(println("Last word length: " + lengthOfLastWord(s)))
    } yield ()
}