package com.d_m
import cats.effect.IO
import scala.io.StdIn

object EditDistance {
  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy[T](expr)
  }

  // 2D Dynamic programming where table[i][j] is the number of steps
  // to change from word1 to word2 where i - 1 is the last character
  // of word1 and j - 1 is the last character of word2.
  //
  // Example:
  //
  //              i - 1
  //                V
  // word1: h o r s e
  //
  //          j - 1
  //            V
  // word2: r o s
  //
  // There are two cases:
  // 1. If the last character of both words are the same, both words can
  // move to the next character without adding steps to change anything.
  // So minDistance("road", "walked") == minDistance("roa", "walke")
  //
  // table[i][j] = table[i - 1][j - 1]
  //
  // 2. If the last character of both words are not the same, there are
  // three options:
  //
  //    * Change the last character of word2 to the last character of word1.
  //        table[i - 1][j - 1] + 1
  //    * Insert the last character of word2 to the end of word1.
  //        table[i][j - 1] + 1
  //    * Insert the last character of word1 to the end of word2.
  //        table[i - 1][j] + 1
  //
  // The best option is the one that minimizes the steps so:
  //
  // table[i][j] = min(table[i - 1][j - 1], table[i][j - 1], table[i - 1][j]) + 1
  def minDistance(word1: String, word2: String): Int = {
    def build(i: Int, j: Int): Lazy[Int] = Lazy {
      if (i == 0)
        j
      else if (j == 0)
        i
      else if (word1(i - 1) == word2(j - 1))
        table(i - 1)(j - 1)()
      else
        table(i)(j - 1)().min(table(i - 1)(j)()).min(table(i - 1)(j - 1)()) + 1
    }
    lazy val table = Array.tabulate(word1.length + 1, word2.length + 1)(build)
    table(word1.length)(word2.length)()
  }

  def run: IO[Unit] =
    for {
      _     <- IO(println("Enter the first word:"))
      word1 <- IO(StdIn.readLine)
      _     <- IO(println("Enter the second word:"))
      word2 <- IO(StdIn.readLine)
      _     <- IO(println("It takes " + minDistance(word1, word2) +
        " steps to change from " + word1 + " to " + word2))
    } yield ()
}