package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object TwoSum {
  def twoSum(nums: Array[Int], target: Int): Array[Int] = {
    val dict = nums.map(target - _).zipWithIndex.toMap
    @tailrec def go(nums: Array[Int], i: Int): Array[Int] = dict.get(nums(i)) match {
      case Some(j) if j != i => Array(i, j)
      case _ => go(nums, i + 1)
    }
    go(nums, 0)
  }

  def run: IO[Unit] =
    for {
      _      <- IO(println("Enter the array separated by spaces:"))
      arrStr <- IO(StdIn.readLine())
      nums    = arrStr.split(' ').map(_.toInt)
      _      <- IO(println("Enter the target:"))
      target <- IO(StdIn.readInt())
      result  = twoSum(nums, target)
      _      <- IO(println("Result: " + result.mkString(" ")))
    } yield ()
}

