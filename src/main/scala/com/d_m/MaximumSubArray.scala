package com.d_m

import cats.effect.IO

import scala.io.StdIn

object MaximumSubArray {
  def maxSubArray(nums: Array[Int]): Int =
    (1 until nums.length).foldLeft((nums(0), nums(0))) {
      case ((prevSum, max), n) =>
        // If the nth element is greater than the nth element + previous sum
        // then ignore the previous sum.
        val sum = if (prevSum <= 0) nums(n) else prevSum + nums(n)
        (sum, max.max(sum))
    }._2

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter an array of integers separated by spaces:"))
      nums <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _    <- IO(println("Maximum subarray sum is: " + maxSubArray(nums)))
    } yield ()
}
