package com.d_m
import cats.effect.IO
import scala.io.StdIn

object PlusOne {
  def plusOne(digits: List[Int]): List[Int] = {
    def go(digits: List[Int]): (List[Int], Int) = digits match {
      case Nil => (Nil, 1)
      case head :: rest =>
        val (result, carry) = go(rest)
        val (sum, newCarry) = ((head + carry) % 10, (head + carry) / 10)
        (sum :: result, newCarry)
    }
    val (result, carry) = go(digits)
    if (carry != 0) carry :: result else result
  }

  def plusOne(digits: Array[Int]): Array[Int] =
    plusOne(digits.toList).toArray

  def run: IO[Unit] =
    for {
      _      <- IO(println("Enter a list of digits separated by spaces:"))
      digits <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _      <- IO(println("After adding one: " + plusOne(digits).mkString(" ")))
    } yield ()
}