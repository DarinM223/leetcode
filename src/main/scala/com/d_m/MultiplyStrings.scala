package com.d_m

import cats.effect.IO

import scala.io.StdIn

object MultiplyStrings {
  // 1. Reverse num1 and num2.
  // 2. Create an array of length num1.length + num2.length initialized to 0.
  // 3. For every i in num1 and every j in num2 set num1(i) * num2(j) at index i + j.
  // 4. For every number in the result array, split into digit and carry and add the carry
  // to the next number in the array, and add the digit to a list.
  // 5. Remove all leading zeroes in the list and convert into a string.
  //
  // Note: num1(i) * num2(j) being at index i + j only works if i and j go from
  // right to left in num1 and num2. So the 0 index will be the rightmost digit in the string.
  // Because all the indexes depend on this reversed indexing scheme, num1 and num2 should
  // be reversed at the start and everything after should use the reversed num1 and num2.
  def multiply(num1: String, num2: String): String = {
    def removeFrontZeroes(l: List[Char]): List[Char] = l match {
      case '0' :: h :: rest => removeFrontZeroes(h :: rest)
      case _ => l
    }

    val (n1, n2) = (num1.reverse, num2.reverse)

    // Total length of n1 * n2 is n1.length + n2.length
    val resultLength = n1.length + n2.length
    val init = (0 until resultLength).map((_, 0)).toMap

    // For index i in n1 and index j in n2, the two digits
    // of n1(i) * n2(j) will be at indexes i + j and i + j + 1.
    // There can be multiple of the same (i, j) index so accumulate all
    // of the results at i + j.
    val indexes =
      for {
        a <- n1.indices
        b <- n2.indices
      } yield (a, b)
    val map = indexes.foldLeft(init) {
      case (map, (i, j)) =>
        map + ((i + j) -> (map(i + j) + (n1(i) - '0') * (n2(j) - '0')))
    }

    val (list, _) = (0 until resultLength).foldLeft((List[Char](), map)) {
      case ((l, map), i) =>
        val digit = ('0'.toInt + (map(i) % 10)).toChar
        val carry = map(i) / 10
        // Add carry to the next digit if it exists
        if (i + 1 < resultLength)
          (digit :: l, map + ((i + 1) -> (map(i + 1) + carry)))
        else
          (digit :: l, map)
    }
    removeFrontZeroes(list).mkString("")
  }

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter the first string of digits:"))
      num1 <- IO(StdIn.readLine).map(_.filter(_.isDigit))
      _    <- IO(println("Enter the second string of digits:"))
      num2 <- IO(StdIn.readLine).map(_.filter(_.isDigit))
      _    <- IO(println(num1 + " * " + num2 + " = " + multiply(num1, num2)))
    } yield ()
}
