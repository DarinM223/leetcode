package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object ThreeSumClosest {
  // First sort the numbers. Then for every element i create a
  // sliding window bounded by i + 1 and nums.length - 1.
  // Then iteratively shorten the bound by the left or right
  // until they cross over each other, keeping track of the sum
  // with the shortest distance from the target.
  def threeSumClosest(nums: Array[Int], target: Int): Int = {
    val n = nums.sorted
    @tailrec def go(i: Int, left: Int, right: Int, shortestDistance: Int, currClosest: Int): Int =
      if (i >= n.length - 2) {
        currClosest
      } else if (left >= right) {
        go(i + 1, i + 2, n.length - 1, shortestDistance, currClosest)
      } else {
        val sum = n(i) + n(left) + n(right)
        if (sum == target) {
          sum
        } else if (sum > target) {
          if (sum - target < shortestDistance)
            go(i, left, right - 1, sum - target, sum)
          else
            go(i, left, right - 1, shortestDistance, currClosest)
        } else {
          if (target - sum < shortestDistance)
            go(i, left + 1, right, target - sum, sum)
          else
            go(i, left + 1, right, shortestDistance, currClosest)
        }
      }
    val initSum = n(0) + n(1) + n(2)
    go(0, 1, n.length - 1, (target - initSum).abs, initSum)
  }

  def run: IO[Unit] =
    for {
      _      <- IO(println("Enter the array of numbers (separated by spaces):"))
      nums   <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _      <- IO(println("Enter the target:"))
      target <- IO(StdIn.readInt)
      _      <- IO(println("Result: " + threeSumClosest(nums, target)))
    } yield ()
}
