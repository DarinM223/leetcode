package com.d_m

import cats.effect.IO
import cats.implicits._

import scala.io.StdIn

object ValidSudoku {
  def isValidSudoku(board: Array[Array[Char]]): Boolean = {
    def rowCells(row: Int): Seq[(Int, Int)] = Seq.range(0, 9).map((row, _))
    def colCells(col: Int): Seq[(Int, Int)] = Seq.range(0, 9).map((_, col))
    def boxCells(startRow: Int, startCol:  Int): Seq[(Int, Int)] =
      for {
        row <- Seq.range(startRow, startRow + 3)
        col <- Seq.range(startCol, startCol + 3)
      } yield (row, col)
    def check(points: Seq[(Int, Int)]): Boolean =
      points.foldRight((true, Set[Char]())) {
        case (_, (false, s)) => (false, s)
        case ((r, c), (true, set)) =>
          if (board(r)(c).isDigit)
            (!set.contains(board(r)(c)), set + board(r)(c))
          else
            (true, set)
      }._1

    lazy val checkRows = Seq.range(0, 9).map(rowCells).count(check) == 9
    lazy val checkCols = Seq.range(0, 9).map(colCells).count(check) == 9
    val boxes =
      for {
        rowIdx <- Seq.range(0, 3)
        colIdx <- Seq.range(0, 3)
      } yield (rowIdx * 3, colIdx * 3)
    lazy val checkBoxes = boxes.map { case (r, c) => boxCells(r, c) }.count(check) == 9
    checkRows && checkCols && checkBoxes
  }

  def validateGrid(grid: Array[Array[Char]]): Boolean = {
    lazy val checkValues =
      ((0 until 9).toList, (0 until 9).toList)
        .mapN((_, _))
        .count { case (r, c) => grid(r)(c).isDigit || grid(r)(c) == '.' } == 81
    grid.length == 9 && grid.count(_.length == 9) == 9 && checkValues
  }

  def promptGrid: IO[Array[Array[Char]]] =
    for {
      _         <- IO(println("Enter 9x9 grid with digits or '.' over 9 lines (with each line being a row):"))
      grid      <- IO(StdIn.readLine).replicateA(9).map(_.map(_.toCharArray).toArray)
      validated <- if (validateGrid(grid)) IO.pure(grid) else IO(println("Invalid grid")) >> promptGrid
    } yield validated

  def run: IO[Unit] =
    for {
      grid <- promptGrid
      _    <- IO(println("Result for valid sudoku is: " + isValidSudoku(grid)))
    } yield ()
}
