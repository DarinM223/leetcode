package com.d_m

import cats.effect.IO

import scala.io.StdIn

// Because leetcode tests are crazy for this problem and a ton
// of stack overflows happen without using a trampoline :(
sealed abstract class Trampoline[+A] {
  def flatMap[B](f: A => Trampoline[B]): Trampoline[B] = this match {
    case FlatMap(a, g) => FlatMap(a, (x: Any) => g(x).flatMap(f))
    case x => FlatMap(x, f)
  }
  def resume: Either[() => Trampoline[A], A] = this match {
    case Done(v) => Right(v)
    case More(k) => Left(k)
    case FlatMap(a, f) => a match {
      case Done(v) => f(v).resume
      case More(k) => Left(() => k().flatMap(f))
      case FlatMap(b, g) =>
        b.flatMap((x: Any) => g(x).flatMap(f)).resume
    }
  }
  def map[B](f: A => B): Trampoline[B] = flatMap(a => Done(f(a)))
  def runT: A = resume match {
    case Right(a) => a
    case Left(k) => k().runT
  }
}
case class Done[+A](value: A) extends Trampoline[A]
case class More[+A](call: () => Trampoline[A]) extends Trampoline[A]
case class FlatMap[A, +B](sub: Trampoline[A], k: A => Trampoline[B]) extends Trampoline[B]

object MergeKSortedLists {
  object ListNode {
    def toList(l: ListNode): List[Int] =
      if (l == null) List() else l.x :: toList(l.next)

    def fromList(l: List[Int]): ListNode = fromListT(l).runT

    private def fromListT(l: List[Int]): Trampoline[ListNode] = l match {
      case Nil => Done(null)
      case x :: rest =>
        More(() => fromListT(rest)).map { rest =>
          val node = new ListNode(x)
          node.next = rest
          node
        }
    }
  }

  def mergeTwoLists(l1: List[Int], l2: List[Int]): Trampoline[List[Int]] = (l1, l2) match {
    case (h1 :: r1, h2 :: r2) if h1 < h2 => More(() => mergeTwoLists(r1, h2 :: r2)).map(h1 :: _)
    case (h1 :: r1, h2 :: r2) => More(() => mergeTwoLists(h1 :: r1, r2)).map(h2 :: _)
    case (l, Nil) => Done(l)
    case (Nil, l) => Done(l)
  }

  def mergeKLists(lists: Array[ListNode]): ListNode = {
    import ListNode._
    fromList(mergeKLists(lists.map(toList)))
  }

  // Separate lists into pairs, merge pairs, then repeat with smaller list of lists.
  // Divide and conquer, similar to merge sort.
  def mergeKLists(lists: Array[List[Int]]): List[Int] = {
    def go(lists: List[List[Int]]): List[List[Int]] =
      if (lists.length == 1) {
        lists
      } else {
        def pairs(lists: List[List[Int]]): Trampoline[List[(List[Int], List[Int])]] = lists match {
          case h1 :: h2 :: rest => More(() => pairs(rest)).map((h1, h2) :: _)
          case h :: Nil => Done(List((h, Nil)))
          case Nil => Done(Nil)
        }
        go(pairs(lists).runT.map { case (l1, l2) => mergeTwoLists(l1, l2).runT })
      }
    if (lists.isEmpty)
      List()
    else
      go(lists.toList).head
  }

  def parseList(s: String): List[Int] =
    s.split(',').map(_.toInt).toList

  def promptLists: IO[Array[List[Int]]] =
    for {
      _ <- IO(println(s"""
Enter an array of lists where the array is separated by spaces
and the list is separated by ',' (Example: \"1,4,5 1,3,4 2,6\")
        """))
      r <- IO(StdIn.readLine).map(_.split(' ').map(parseList))
    } yield r

  def run: IO[Unit] = promptLists.flatMap { lists =>
    IO(println("Merged list: " + mergeKLists(lists).mkString(",")))
  }
}
