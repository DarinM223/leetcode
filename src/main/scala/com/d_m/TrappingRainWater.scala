package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object TrappingRainWater {
  type Stack = List[(Int, Int)]

  // Brute force: for every index in height, search the left side for the biggest
  // height (left_max), and right side for the biggest height (right_max),
  // then add min(max_left, max_height) - height[i] to the count.

  // Use dynamic programming to store the maximum height from the left side
  // and the maximum height from the right side for each index.
  def trapDP(height: Array[Int]): Int =
    if (height.isEmpty) {
      0
    } else {
      val initLeftMax = Map(0 -> height(0))
      val initRightMax = Map((height.length - 1) -> height(height.length - 1))
      val leftMax = (1 until height.length).foldLeft(initLeftMax) { (leftMax, i) =>
        leftMax + (i -> height(i).max(leftMax(i - 1)))
      }
      val rightMax = (0 until height.length - 1).foldRight(initRightMax) { (i, rightMax) =>
        rightMax + (i -> height(i).max(rightMax(i + 1)))
      }
      (1 until height.length - 1).foldLeft(0) { (count, i) =>
        count + leftMax(i).min(rightMax(i)) - height(i)
      }
    }

  // Use a stack to keep track of the maxes from the left.
  def trapStack(height: Array[Int]): Int = {
    @tailrec def collapseStack(count: Int, stack: Stack, h: Int, i: Int): (Int, Stack) = stack match {
      case (oldTopHeight, _) :: tail if h > oldTopHeight => tail match {
        case (newTopHeight, newTopIdx) :: _ =>
          // If there is another top after the top is popped,
          // add the trapped water to the result.
          val distance = i - newTopIdx - 1
          val boundedHeight = h.min(newTopHeight) - oldTopHeight
          collapseStack(count + distance * boundedHeight, tail, h, i)
        case Nil => (count, (h, i) :: tail)
      }
      case _ => (count, (h, i) :: stack)
    }
    height.zipWithIndex.foldLeft((0, List[(Int, Int)]())) {
      case ((count, stack), (h, i)) => collapseStack(count, stack, h, i)
    }._1
  }

  // Two pointers, one on the left side, one on the right side of the array.
  // Don't need to store every single left_max, right_max if you only increment
  // the pointer at the side that has the smaller max at the time.
  //
  // Example:
  //                  right
  // left              |
  //  |   i  -----     V
  //  V   |  |   |    ----
  // ---- |  |   |    |  |
  //    | V  |   |    |  |
  // ------------------------
  //                  right
  // left              |
  //  |   i            V
  //  V   |           ----
  // ---- |           |  |
  //    | V           |  |
  // -------------------------
  //
  // height(left) < height(right) so the water level at i is going to be
  // based off of the left side regardless if there is something in the middle
  // that is bigger than the height at left or right.
  def trap2Pointer(height: Array[Int]): Int = {
    @tailrec def go(left: Int, right: Int, leftMax: Int, rightMax: Int, count: Int): Int =
      if (left < right) {
        if (height(left) < height(right)) {
          if (height(left) >= leftMax)
            go(left + 1, right, height(left), rightMax, count)
          else
            go(left + 1, right, leftMax, rightMax, count + (leftMax - height(left)))
        } else {
          if (height(right) >= rightMax)
            go(left, right - 1, leftMax, height(right), count)
          else
            go(left, right - 1, leftMax, rightMax, count + (rightMax - height(right)))
        }
      } else {
        count
      }
    go(0, height.length - 1, 0, 0, 0)
  }

  val approaches: Map[String, Array[Int] => Int] = Map(
    "dynamic programming" -> trapDP,
    "stack"               -> trapStack,
    "two pointers"        -> trap2Pointer,
  )

  def run: IO[Unit] =
    for {
      fn     <- IOUtils.promptApproach(approaches)
      _      <- IO(println("Enter the array of height integers separated by spaces:"))
      height <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _      <- IO(println("The water trapped is: " + fn(height)))
    } yield ()
}
