package com.d_m

import cats.effect.IO

import scala.io.StdIn

object JumpGame {
  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy[T](expr)
  }

  def canJump(nums: Array[Int]): Boolean = {
    def build(i: Int): Lazy[Boolean] = Lazy {
      if (i == nums.length - 1)
        true
      else if (nums(i) == 0)
        false
      else
        (i + 1 to (i + nums(i))).count(i => i < nums.length && table(i)()) > 0
    }
    lazy val table = Array.tabulate(nums.length)(build)
    table(0)()
  }

  // Greedy optimization
  // You only need one successful route in order for canJump to succeed.
  // Because of this you don't need to store every possible route.
  // Only need to store the leftmost GOOD position iterating from right to left.
  // A GOOD position is one that can reach the rightmost element through jumps.
  //
  // Example: for input array [9, 4, 2, 1, 0, 2, 0]
  // U stands for UNKNOWN, G stands for GOOD, and B stands for BAD.
  //
  // Index   | 0 | 1 | 2 | 3 | 4 | 5 | 6
  // --------+---+---+---+---+---+---+--
  // Numbers | 9 | 4 | 2 | 1 | 0 | 2 | 0
  // --------+---+---+---+---+---+---+--
  // Memo    | U | G | B | B | B | G | G
  //
  // Index 0 will be marked as GOOD because it can reach index 1 which is the
  // leftmost GOOD index. Indexes 5 and 6 aren't important because index 1 has
  // already been proven to be able to reach to them through jumps.
  def canJumpGreedy(nums: Array[Int]): Boolean =
    nums.indices.foldRight(nums.length - 1) { (i, lastGoodIndex) =>
      // If the last good index is reachable at index i
      // then index i is the new last good index
      if (i + nums(i) >= lastGoodIndex) i else lastGoodIndex
    } == 0 // Check that the last good index is the first index.

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter an array of integers separated by spaces:"))
      nums <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _    <- IO(println("Result: " + canJumpGreedy(nums)))
    } yield ()
}
