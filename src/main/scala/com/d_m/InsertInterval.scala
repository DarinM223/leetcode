package com.d_m

import cats.effect.IO

import scala.io.StdIn

object InsertInterval {
  implicit class IntervalOps(interval: Array[Int]) {
    def start: Int = interval(0)
    def end: Int = interval(1)
  }

  def insert(
    intervals: Array[Array[Int]],
    newInterval: Array[Int]
  ): Array[Array[Int]] = {
    val (results, newInterval2) = 
      intervals.foldLeft((List[Array[Int]](), newInterval)) {
        // In this case, the current interval is before the interval to insert
        // and since it doesn't overlap with the new interval, it can be appended
        // directly to the results.
        case ((l, newInterval), interval) if interval.end < newInterval.start =>
          (interval :: l, newInterval)
        // In this case, the current interval is after the interval to insert
        // and it doesn't overlap with the interval to insert. Append the interval
        // to insert, and make the current interval the new interval to insert,
        // since the current interval is now the interval that might be extended.
        case ((l, newInterval), interval) if interval.start > newInterval.end =>
          (newInterval :: l, interval)
        // In this case, the current interval overlaps with the interval to insert,
        // so the interval to insert gets extended with the current interval, and
        // nothing gets appended to the results.
        case ((l, newInterval), interval)
          if interval.end >= newInterval.start ||
             interval.start <= newInterval.end =>
          val updatedInterval = Array(
              interval.start.min(newInterval.start),
              interval.end.max(newInterval.end)
          )
          (l, updatedInterval)
      }
    // The last interval to insert won't be appended at the end
    // of the fold, so append it here.
    (newInterval2 :: results).reverse.toArray
  }

  def run: IO[Unit] =
    for {
      _           <- IO(println("Enter an array of non overlapping intervals sorted by start:"))
      _           <- IO(println("Example: 1,3 6,9"))
      intervals   <- IO(StdIn.readLine).map(_.split(' ').map(_.split(',').map(_.toInt)))
      _           <- IO(println("Enter interval to insert (Example: 2,5):"))
      newInterval <- IO(StdIn.readLine).map(_.split(',').map(_.toInt))
      _           <- IO(println("New intervals: " +
        insert(intervals, newInterval).map(_.mkString(",")).mkString(" ")))
    } yield ()
}