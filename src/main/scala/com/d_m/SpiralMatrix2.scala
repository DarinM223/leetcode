package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object SpiralMatrix2 {
  sealed abstract class Direction {
    def move(position: (Int, Int)): (Int, Int) = (this, position) match {
      case (Left, (row, col)) => (row, col - 1)
      case (Right, (row, col)) => (row, col + 1)
      case (Up, (row, col)) => (row - 1, col)
      case (Down, (row, col)) => (row + 1, col)
    }

    def change: Direction = this match {
      case Right => Down
      case Down => Left
      case Left => Up
      case Up => Right
    }
  }
  final case object Left extends Direction
  final case object Right extends Direction
  final case object Up extends Direction
  final case object Down extends Direction

  // My solution that passes leetcode but is inefficient due to storing
  // visited values in a Set.
  def generateMatrix(n: Int): Array[Array[Int]] = {
    def outOfBounds(position: (Int, Int), visited: Set[(Int, Int)]): Boolean =
      position._1 < 0 || position._2 < 0 ||
      position._1 >= n || position._2 >= n ||
      visited.contains(position)

    @tailrec def go(
      table: Array[Array[Int]],
      visited: Set[(Int, Int)],
      position: (Int, Int),
      counter: Int,
      direction: Direction
    ): Array[Array[Int]] =
      if (outOfBounds(position, visited)) {
        table
      } else {
        table(position._1)(position._2) = counter
        val nextPosition = direction.move(position)
        if (outOfBounds(nextPosition, visited))
          go(table, visited + position, direction.change.move(position), counter + 1, direction.change)
        else
          go(table, visited + position, nextPosition, counter + 1, direction)
      }
    go(Array.fill(n, n)(0), Set(), (0, 0), 1, Right)
  }

  // Goes layer by layer similar to rotating matrix.
  // Step determines the current layer and all the dimensions
  // are offsets of step.
  def generateMatrixStep(n: Int): Array[Array[Int]] = {
    val result = Array.fill(n, n)(0)
    var x = 0
    var y = 0
    var step = 0
    var i = 0
    while (i < n * n) {
      while (x + step < n) {
        i += 1
        result(y)(x) = i
        x += 1
      }
      x -= 1
      y += 1
      while (y + step < n) {
        i += 1
        result(y)(x) = i
        y += 1
      }
      y -= 1
      x -= 1
      while (x >= step) {
        i += 1
        result(y)(x) = i
        x -= 1
      }
      x += 1
      y -= 1
      step += 1 // Increment step here so that it doesn't loop back to start.
      while (y >= step) {
        i += 1
        result(y)(x) = i
        y -= 1
      }
      y += 1
      x += 1
    }
    result
  }

  // Keeps track of all four dimensions instead of a single step variable.
  // Shrinks the current dimension by 1 every time after iterating over it.
  def generateMatrixDimensions(n: Int): Array[Array[Int]] = {
    val result = Array.fill(n, n)(0)
    var k = 1
    var top = 0
    var bottom = n - 1
    var left = 0
    var right = n - 1
    while (k <= n * n) {
      for (i <- left to right) {
        result(top)(i) = k
        k += 1
      }
      top += 1
      for (i <- top to bottom) {
        result(i)(right) = k
        k += 1
      }
      right -= 1
      for (i <- (left to right).reverse) {
        result(bottom)(i) = k
        k += 1
      }
      bottom -= 1
      for (i <- (top to bottom).reverse) {
        result(i)(left) = k
        k += 1
      }
      left += 1
    }
    result
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter an integer n to generate an n by n matrix:"))
      n <- IO(StdIn.readInt)
      _ <- IO(println("Matrix is: \n" + generateMatrixStep(n).map(_.mkString(" ")).mkString("\n")))
    } yield ()
}