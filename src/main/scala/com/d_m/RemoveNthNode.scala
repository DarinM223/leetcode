package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object RemoveNthNode {
  def removeNthFromEnd(head: ListNode, n: Int): ListNode = {
    import ListNode._
    fromList(removeNthFromEnd(toList(head), n))
  }

  def removeNthFromEnd(head: List[Int], n: Int): List[Int] = {
    @tailrec def nthNodeOffset(l: List[Int], n: Int): List[Int] = l match {
      case _ :: _ if n == 0 => l
      case _ :: rest => nthNodeOffset(rest, n - 1)
      case Nil => Nil
    }
    def go(start: List[Int], end: List[Int], foundNth: Boolean): List[Int] = (start, end) match {
      case (_ :: rest, Nil) if !foundNth => go(rest, Nil, true)
      case (l, Nil) => l
      case (h1 :: rest1, _ :: rest2) => h1 :: go(rest1, rest2, foundNth)
    }
    go(head, nthNodeOffset(head, n), false)
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter the list of numbers (separated by spaces):"))
      l <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt).toList)
      _ <- IO(println("Enter the offset n (from the end of the list):"))
      n <- IO(StdIn.readInt)
      _ <- IO(println("List after the nth node from end is removed: " + removeNthFromEnd(l, n)))
    } yield ()
}
