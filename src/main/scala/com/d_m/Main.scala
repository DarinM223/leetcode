package com.d_m

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._

import scala.io.StdIn

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] =
    promptProblem.flatMap(runProblem).as(ExitCode.Success)

  val problems = Map(
    "two sum"                   -> TwoSum.run,
    "add two numbers"           -> AddTwoNumbers.run,
    "longest substring"         -> LongestSubstring.run,
    "median two sorted"         -> MedianTwoSorted.run,
    "longest palindrome"        -> LongestPalindrome.run,
    "zigzag conversion"         -> ZigZag.run,
    "reverse integer"           -> ReverseInteger.run,
    "atoi"                      -> Atoi.run,
    "regex matching"            -> Regex.run,
    "container most water"      -> MostWater.run,
    "integer to roman"          -> IntToRoman.run,
    "roman to integer"          -> RomanToInt.run,
    "longest common prefix"     -> LongestCommonPrefix.run,
    "three sum"                 -> ThreeSum.run,
    "three sum closest"         -> ThreeSumClosest.run,
    "letter combinations"       -> LetterCombinations.run,
    "four sum"                  -> FourSum.run,
    "remove nth node"           -> RemoveNthNode.run,
    "valid parenthesis"         -> ValidParenthesis.run,
    "merge sorted lists"        -> MergeSortedLists.run,
    "generate parentheses"      -> GenerateParentheses.run,
    "merge k sorted lists"      -> MergeKSortedLists.run,
    "swap nodes in pairs"       -> SwapNodesInPairs.run,
    "remove duplicates sorted"  -> RemoveDupsSorted.run,
    "remove element"            -> RemoveElement.run,
    "strStr"                    -> Strstr.run,
    "divide two integers"       -> DivideTwoIntegers.run,
    "next permutation"          -> NextPermutation.run,
    "longest valid parentheses" -> LongestValidParens.run,
    "search rotated array"      -> SearchRotatedArray.run,
    "find first last in sorted" -> FindFirstLastSorted.run,
    "search insert position"    -> SearchInsertPosition.run,
    "valid sudoku"              -> ValidSudoku.run,
    "count and say"             -> CountAndSay.run,
    "combination sum"           -> CombinationSum.run,
    "combination sum 2"         -> CombinationSum2.run,
    "first missing positive"    -> FirstMissingPositive.run,
    "trapping rain water"       -> TrappingRainWater.run,
    "multiply strings"          -> MultiplyStrings.run,
    "wildcard matching"         -> WildcardMatching.run,
    "jump game 2"               -> JumpGame2.run,
    "permutations"              -> Permutations.run,
    "permutations 2"            -> Permutations2.run,
    "group anagrams"            -> GroupAnagrams.run,
    "pow"                       -> Pow.run,
    "n queens"                  -> NQueens.run,
    "n queens 2"                -> NQueens2.run,
    "maximum subarray"          -> MaximumSubArray.run,
    "spiral matrix"             -> SpiralMatrix.run,
    "jump game"                 -> JumpGame.run,
    "merge intervals"           -> MergeIntervals.run,
    "insert interval"           -> InsertInterval.run,
    "length of last word"       -> LengthOfLastWord.run,
    "spiral matrix 2"           -> SpiralMatrix2.run,
    "rotate list"               -> RotateList.run,
    "unique paths"              -> UniquePaths.run,
    "unique paths 2"            -> UniquePaths2.run,
    "minimum path sum"          -> MinimumPathSum.run,
    "plus one"                  -> PlusOne.run,
    "add binary"                -> AddBinary.run,
    "sqrt"                      -> Sqrt.run,
    "climbing stairs"           -> ClimbingStairs.run,
    "simplify path"             -> SimplifyPath.run,
    "edit distance"             -> EditDistance.run,
    "set matrix zeroes"         -> SetMatrixZeroes.run,
  )

  def promptProblem: IO[String] =
    IO(println("Enter the problem name:")) *> IO(StdIn.readLine())

  def runProblem(problem: String): IO[Unit] = problems.get(problem) match {
    case Some(run) => run
    case None =>
      for {
        _       <- IO(println("Available Problems: " + problems.keys.mkString(", ")))
        problem <- promptProblem
        _       <- runProblem(problem)
      } yield ()
  }
}
