package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object RomanToInt {
  def romanToInt(s: String): Int = {
    val values = Map(
      'I' -> 1,
      'V' -> 5,
      'X' -> 10,
      'L' -> 50,
      'C' -> 100,
      'D' -> 500,
      'M' -> 1000
    )
    @tailrec def go(i: Int, acc: Int): Int =
      if (i >= s.length) {
        acc
      } else {
        val firstValue = values.getOrElse(s(i), 0)
        if (i + 1 < s.length && values.getOrElse(s(i + 1), 0) > firstValue)
          go(i + 1, acc - firstValue)
        else
          go(i + 1, acc + firstValue)
      }
    if (s.isEmpty)
      0
    else
      go(0, 0)
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter the roman numerals to convert to integer:"))
      s <- IO(StdIn.readLine)
      _ <- IO(println("Result: " + romanToInt(s)))
    } yield ()
}
