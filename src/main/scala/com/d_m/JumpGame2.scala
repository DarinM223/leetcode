package com.d_m

import cats.effect.IO

import scala.io.StdIn

object JumpGame2 {
  // Iterate through the indexes (not including the last index)
  // keeping track of the farthest index reachable at the time.
  // When the index hits the current end index, update the end index to the farthest index
  // reachable and increment the # of jumps.
  //
  // Example: [2, 3, 1, 1, 4]
  //
  // jumps: 0, end: 0, farthest: 0
  // [2, 3, 1, 1, 4]
  //  ^ (index == end)
  // jumps: 1, end: 2, farthest: 2 (0 + 2)
  // [2, 3, 1, 1, 4]
  //     ^
  // jumps: 1, end: 2, farthest: 4 (1 + 3)
  // [2, 3, 1, 1, 4]
  //        ^ (index == end)
  // jumps: 2, end: 4, farthest: 4
  // [2, 3, 1, 1, 4]
  //           ^
  // Terminates here because next index is nums.length - 1
  def jump(nums: Array[Int]): Int =
    (0 until nums.length - 1).foldLeft((0, 0, 0)) {
      case ((jumps, end, farthest), i) =>
        val newFarthest = farthest.max(i + nums(i))
        if (i == end)
          (jumps + 1, newFarthest, newFarthest)
        else
          (jumps, end, newFarthest)
    }._1

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter an array of non-negative integers separated by spaces:"))
      nums <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _    <- IO(println("Minimum jumps is: " + jump(nums)))
    } yield ()
}
