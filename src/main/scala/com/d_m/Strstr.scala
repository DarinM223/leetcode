package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object Strstr {
  def strStr(haystack: String, needle: String): Int = {
    // i indexes into haystack, j indexes into needle.
    // i - j is the haystack position at the start of the current match.
    // when current match fails haystack index goes back to
    // (start of current match + 1) and needle index goes back to 0.
    @tailrec def go(i: Int, j: Int): Int =
      if (j >= needle.length)
        i - j
      else if (i >= haystack.length)
        -1
      else if (haystack(i) == needle(j))
        go(i + 1, j + 1)
      else
        go(i - j + 1, 0)
    if (needle.isEmpty)
      0
    else
      go(0, 0)
  }

  def run: IO[Unit] =
    for {
      _        <- IO(println("Enter the haystack string:"))
      haystack <- IO(StdIn.readLine)
      _        <- IO(println("Enter the needle string:"))
      needle   <- IO(StdIn.readLine)
      _        <- IO(println("Match starts at index: " + strStr(haystack, needle)))
    } yield ()
}
