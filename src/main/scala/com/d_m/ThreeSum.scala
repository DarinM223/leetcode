package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object ThreeSum {
  def threeSum(nums: Array[Int]): List[List[Int]] = {
    val arr = nums.sorted
    val n = nums.length
    @tailrec def go(x: Int, l: Int, r: Int, acc: Set[List[Int]]): Set[List[Int]] =
      if (l >= r) {
        acc
      } else {
        val sum = x + arr(l) + arr(r)
        if (sum == 0)
          go(x, l + 1, r - 1, acc + List(x, arr(l), arr(r)).sorted)
        else if (sum < 0)
          go(x, l + 1, r, acc)
        else
          go(x, l, r - 1, acc)
      }
    (0 until n - 1)
      .foldLeft(Set[List[Int]]())((acc, i) => go(arr(i), i + 1, n - 1, acc))
      .toList
  }

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter the array of numbers (separated by spaces):"))
      nums <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _    <- IO(println("Result: " + threeSum(nums)))
    } yield ()
}
