package com.d_m

import cats.effect.IO

import scala.io.StdIn

object NQueens2 {
  // Same thing as N Queens but store the queens more efficiently
  // with sets.
  //
  // To check if two points are on the same left-to-right diagonal, check
  // if their row - col values are equal.
  // Example:
  // (0, 0) and (1, 1) are on the same diagonal and 1 - 1 = 0 - 0
  // (0, 1) and (1, 2) are on the same diagonal and 0 - 1 = 1 - 2
  //
  // To check if two points are on the same right-to-left diagonal, check
  // if their row + col values are equal.
  // Example:
  // (0, 2) and (1, 1) are on the same diagonal and 0 + 2 = 1 + 1
  // (3, 4) and (4, 3) are on the same diagonal and 3 + 4 = 4 + 3
  def totalNQueens(n: Int): Int = {
    def check(row: Int, col: Int, cols: Set[Int], diag1: Set[Int], diag2: Set[Int]): Boolean =
      !cols.contains(col) && !diag1.contains(row - col) && !diag2.contains(row + col)
    def go(count: Int, row: Int, cols: Set[Int], diag1: Set[Int], diag2: Set[Int]): Int =
      if (row >= n) {
        count + 1
      } else {
        (0 until n).filter(check(row, _, cols, diag1, diag2)).foldLeft(count) { (count, col) =>
          go(count, row + 1, cols + col, diag1 + (row - col), diag2 + (row + col))
        }
      }
    go(0, 0, Set(), Set(), Set())
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter an integer n for placing n queens on a nxn chessboard:"))
      n <- IO(StdIn.readInt)
      _ <- IO(println("Number of solutions is: " + totalNQueens(n)))
    } yield ()
}
