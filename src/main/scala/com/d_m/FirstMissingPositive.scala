package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object FirstMissingPositive {
  def firstMissingPositive(nums: Array[Int]): Int = {
    val arr = nums.clone()
    @tailrec def go(i: Int): Unit =
      if (arr(i) != i + 1) {
        if (arr(i) <= 0 || arr(i) >= arr.length) return
        if (arr(i) == arr(arr(i) - 1)) return

        // Side effects here to swap in place
        val temp = arr(i)
        arr(i) = arr(temp - 1)
        arr(temp - 1) = temp

        go(i)
      }
    arr.indices.foreach(go)
    arr.indices.find(i => arr(i) != i + 1).getOrElse(arr.length) + 1
  }

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter an array of integers separated by spaces:"))
      nums <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _    <- IO(println("First missing positive is: " + firstMissingPositive(nums)))
    } yield ()
}
