package com.d_m
import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object SimplifyPath {
  def simplifyPath(path: String): String = {
    def eatDirectory(path: List[Char]): (String, List[Char]) = {
      def go(path: List[Char]): (List[Char], List[Char]) = path match {
        case '/' :: rest => (Nil, rest)
        case ch :: rest =>
          val (dir, finalRest) = go(rest)
          (ch :: dir, finalRest)
        case Nil => (Nil, Nil)
      }
      val (dir, finalRest) = go(path)
      (dir.mkString, finalRest)
    }
    @tailrec def go(path: List[Char], stack: List[String]): List[String] = path match {
      case ('.' :: '.' :: '/' :: rest) =>
        go(rest, if (stack.isEmpty) stack else stack.tail)
      case ('.' :: '/' :: rest) => go(rest, stack)
      case ('/' :: rest) => go(rest, stack)
      case (_ :: _) =>
        val (s, rest) = eatDirectory(path)
        go(rest, s :: stack)
      case Nil => stack
    }
    def stackToPath(stack: List[String]): String =
      "/" + stack.reverse.mkString("/")
    stackToPath(go((path + "/").toList, Nil))
  }

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter the path:"))
      path <- IO(StdIn.readLine)
      _    <- IO(println("Simplified path is: " + simplifyPath(path)))
    } yield ()
}