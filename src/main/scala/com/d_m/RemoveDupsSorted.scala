package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object RemoveDupsSorted {
  // Two indexes, the first index to indicate where to set the next
  // value and the second index to compare with second index - 1 to
  // check if it is a duplicate.
  //
  // Sadly leetcode doesn't allow cats/scalaz IO and prevents using
  // immutability for this problem so we have to break referential transparency here.
  def removeDuplicates(nums: Array[Int]): Int = {
    @tailrec def go(nums: Array[Int], length: Int, setIdx: Int, currIdx: Int): Int =
      if (currIdx >= nums.length) {
        length
      } else if (nums(currIdx) == nums(currIdx - 1)) {
        go(nums, length, setIdx, currIdx + 1)
      } else {
        nums(setIdx) = nums(currIdx)
        go(nums, length + 1, setIdx + 1, currIdx + 1)
      }
    if (nums.length < 2)
      nums.length
    else
      go(nums, 0, 1, 1) + 1
  }

  def run: IO[Unit] = IO {
    println("Enter the array of integers (separated by spaces):")
    val array = StdIn.readLine.split(' ').map(_.toInt)
    val length = removeDuplicates(array)
    println("Modified array: " + array.mkString(" "))
    println("New length: " + length)
    println("Modified array truncated to new length: " +
      array.slice(0, length).mkString(" "))
  }
}
