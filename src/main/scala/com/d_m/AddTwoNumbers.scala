package com.d_m

import cats.effect.IO

import scala.io.StdIn

class ListNode(var _x: Int = 0) {
  var next: ListNode = null
  var x: Int = _x
}

object ListNode {
  def toList(l: ListNode): List[Int] =
    if (l == null) List() else l.x :: toList(l.next)

  def fromList(l: List[Int]): ListNode = l match {
    case Nil => null
    case x :: rest =>
      val node = new ListNode(x)
      node.next = fromList(rest)
      node
  }
}

object AddTwoNumbers {
  import ListNode._

  def addTwoNumbers(l1: ListNode, l2: ListNode): ListNode =
    fromList(addTwoNumbers(toList(l1), toList(l2)))

  def addTwoNumbers(l1: List[Int], l2: List[Int]): List[Int] = {
    def safeTail[T](l: List[T]): List[T] = l match {
      case Nil       => Nil
      case _ :: rest => rest
    }
    def go(l1: List[Int], l2: List[Int], carry: Int): List[Int] = {
      val v1 = l1.headOption.getOrElse(0)
      val v2 = l2.headOption.getOrElse(0)
      val sum = v1 + v2 + carry
      if (sum == 0 && l1.isEmpty && l2.isEmpty) {
        List()
      } else {
        val result = sum % 10
        val newCarry = sum / 10
        result :: go(safeTail(l1), safeTail(l2), newCarry)
      }
    }
    go(l1, l2, 0)
  }

  def run: IO[Unit] =
    for {
      _  <- IO(println("Enter the first array of numbers (in reverse order):"))
      l1 <- IO(StdIn.readLine()).map(_.split(' ').map(_.toInt).toList)
      _  <- IO(println("Enter the second array of numbers (in reverse order):"))
      l2 <- IO(StdIn.readLine()).map(_.split(' ').map(_.toInt).toList)
      r   = addTwoNumbers(l1, l2)
      _  <- IO(println("Result is: " + r.mkString(" ")))
    } yield ()
}
