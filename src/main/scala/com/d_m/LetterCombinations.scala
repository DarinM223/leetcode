package com.d_m

import cats.effect.IO
import cats.implicits._

import scala.io.StdIn

object LetterCombinations {
  // Similar to permutations of a string without duplicates problem.
  // Difference is that the "choices" are based on the digit to letter mapping
  // in a telephone instead of a hashmap checking valid remaining characters.
  def letterCombinations(digits: String): List[String] = {
    def go(results: List[String], suffix: List[Char], digits: List[Char]): List[String] = digits match {
      case Nil => suffix.mkString :: results
      case digit :: rest =>
        val choices = digit match {
          case '2' => List('a', 'b', 'c')
          case '3' => List('d', 'e', 'f')
          case '4' => List('g', 'h', 'i')
          case '5' => List('j', 'k', 'l')
          case '6' => List('m', 'n', 'o')
          case '7' => List('p', 'q', 'r', 's')
          case '8' => List('t', 'u', 'v')
          case '9' => List('w', 'x', 'y', 'z')
          case _   => List()
        }
        choices.foldLeft(results)((results, choice) => go(results, choice :: suffix, rest))
    }
    if (digits.isEmpty)
      List()
    else
      go(List(), List(), digits.toList.reverse)
  }

  def promptDigits: IO[String] =
    for {
      _      <- IO(println("Enter the string containing digits from 2-9 inclusive:"))
      digits <- IO(StdIn.readLine)
      isDigit = (c: Char) => c.isDigit && Range(2, 10).contains(c.asDigit)
      validatedDigits <-
        if (digits.count(isDigit) == digits.length)
          IO.pure(digits)
        else
          IO(println("String can only contain digits from 2-9 inclusive")) >> promptDigits
    } yield validatedDigits

  def run: IO[Unit] =
    for {
      digits <- promptDigits
      _      <- IO(println("Result: " + letterCombinations(digits).mkString(", ")))
    } yield ()
}
