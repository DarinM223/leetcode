package com.d_m

import cats.effect.IO

import scala.io.StdIn

object CountAndSay {
  // Question is asking how do you say the recursive solution previous number?
  // Example: countAndSay(5)
  // countAndSay(4) = 1211
  // so countAndSay(5) is how you would say 1211 in english:
  // "one one, one two, and two ones"
  // ~> "111221"
  def countAndSay(n: Int): String = n match {
    case 1 => "1"
    case n =>
      val prev = countAndSay(n - 1)
      val l = prev.foldRight(List[(Int, Char)]()) {
        case (ch, Nil) => (1, ch) :: Nil
        case (ch, (count, c) :: rest) if ch == c => (count + 1, c) :: rest
        case (ch, l) => (1, ch) :: l
      }
      l.foldLeft("") { case (s, (count, ch)) => s + count + ch }
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter the integer n:"))
      n <- IO(StdIn.readInt)
      _ <- IO(println("The result is: " + countAndSay(n)))
    } yield ()
}
