package com.d_m

import cats.effect.IO

import scala.io.StdIn

object Regex {
  // When matching the head of the string and pattern:
  //
  // If the next character is '*', then you can skip both
  // the current character and the '*' character in the pattern.
  //
  // If the head characters of the string and pattern matches
  // and the next character is '*', you can stay on the same pattern
  // and move to the next string character.
  def isMatch(s: String, p: String): Boolean =
    if (p.isEmpty) {
      s.isEmpty
    } else {
      val firstMatch = (s.headOption, p.headOption) match {
        case (Some(_), Some('.')) => true
        case (Some(sh), Some(ph)) if sh == ph => true
        case _ => false
      }
      if (p.length >= 2 && p.tail.headOption.contains('*'))
        isMatch(s, p.tail.tail) || (firstMatch && isMatch(s.tail, p))
      else
        firstMatch && isMatch(s.tail, p.tail)
    }

  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy(expr)
  }

  // Caches solutions with dynamic programming.
  def isMatchDP(s: String, p: String): Boolean = {
    def build(i: Int, j: Int): Lazy[Boolean] = Lazy {
      if (j == p.length) {
        i == s.length
      } else {
        val firstMatch = i < s.length &&
          (p.charAt(j) == '.' || p.charAt(j) == s.charAt(i))
        if (j + 1 < p.length && p.charAt(j + 1) == '*') {
          table(i)(j + 2)() || (firstMatch && table(i + 1)(j)())
        } else {
          firstMatch && table(i + 1)(j + 1)()
        }
      }
    }

    lazy val table = Array.tabulate[Lazy[Boolean]](s.length + 1, p.length + 1)(build)
    table(0)(0)()
  }

  val approaches: Map[String, (String, String) => Boolean] = Map(
    "recursive"           -> isMatch,
    "dynamic programming" -> isMatchDP,
  )

  def run: IO[Unit] =
    for {
      f <- IOUtils.promptApproach(approaches)
      _ <- IO(println("Enter the string:"))
      s <- IO(StdIn.readLine)
      _ <- IO(println("Enter the pattern:"))
      p <- IO(StdIn.readLine)
      _ <- IO(println("Match result: " + f(s, p)))
    } yield ()
}
