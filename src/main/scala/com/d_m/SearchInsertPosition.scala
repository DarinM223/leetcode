package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object SearchInsertPosition {
  def searchInsert(nums: Array[Int], target: Int): Int = {
    @tailrec def go(start: Int, end: Int): Int = {
      val mid = (start + end) / 2
      if (end <= start)
        if (target <= nums(mid)) mid else mid + 1
      else if (target == nums(mid))
        mid
      else if (nums(mid) < target)
        go(mid + 1, end)
      else
        go(start, mid - 1)
    }
    go(0, nums.length - 1)
  }

  def run: IO[Unit] =
    for {
      _      <- IO(println("Enter a sorted array of integers with no duplicates (separated by spaces):"))
      nums   <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      _      <- IO(println("Enter target: "))
      target <- IO(StdIn.readInt)
      _      <- IO(println("Insert index is: " + searchInsert(nums, target)))
    } yield ()
}
