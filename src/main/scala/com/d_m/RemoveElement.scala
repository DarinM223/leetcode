package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object RemoveElement {
  // Two indexes, the first index to indicate where to set the next
  // value and the second index to check if the current element equals `val`.
  //
  // Sadly leetcode doesn't allow cats/scalaz IO and prevents using
  // immutability for this problem so we have to break referential transparency here.
  def removeElement(nums: Array[Int], `val`: Int): Int = {
    val target = `val`
    @tailrec def go(nums: Array[Int], length: Int, setIdx: Int, currIdx: Int): Int =
      if (currIdx >= nums.length) {
        length
      } else if (nums(currIdx) == target) {
        go(nums, length - 1, setIdx, currIdx + 1)
      } else {
        nums(setIdx) = nums(currIdx)
        go(nums, length, setIdx + 1, currIdx + 1)
      }
    go(nums, nums.length, 0, 0)
  }

  def run: IO[Unit] = IO {
    println("Enter the array of integers (separated by spaces):")
    val array = StdIn.readLine.split(' ').map(_.toInt)
    println("Enter the number to remove from array:")
    val target = StdIn.readInt
    val length = removeElement(array, target)
    println("Modified array: " + array.mkString(" "))
    println("New length: " + length)
    println("Modified array truncated to new length: " +
      array.slice(0, length).mkString(" "))
  }
}
