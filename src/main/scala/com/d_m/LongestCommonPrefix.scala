package com.d_m

import cats.effect.IO

import scala.io.StdIn

object LongestCommonPrefix {
  def longestCommonPrefix(strs: Array[String]): String =
    if (strs.isEmpty) {
      ""
    } else {
      def go(strs: Array[String], ch: Option[Char]): List[Char] = {
        if (ch.isDefined && strs.count(_.headOption == ch) == strs.length) {
          val updated = strs.map(_.tail)
          ch.head :: go(updated, updated(0).headOption)
        } else {
          Nil
        }
      }
      go(strs, strs(0).headOption).mkString
    }

  def run: IO[Unit] =
    for {
      _    <- IO(println("Enter an array of strings separated by spaces"))
      strs <- IO(StdIn.readLine).map(_.split(' '))
      _    <- IO(println("The longest common prefix is: \"" + longestCommonPrefix(strs) + "\""))
    } yield ()
}
