package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object SearchRotatedArray {
  def search(nums: Array[Int], target: Int): Int = {
    @tailrec def go(start: Int, end: Int): Int = {
      val mid = (start + end) / 2
      if (end < start) {
        -1
      } else if (nums(mid) == target) {
        mid
      } else if (nums(start) <= nums(mid) && nums(end) <= nums(mid)) {
        // If start -> mid is in order and the target is in that range
        // look at left side, otherwise look at right side.
        if ((nums(start) to nums(mid)).contains(target))
          go(start, mid - 1)
        else
          go(mid + 1, end)
      } else {
        // If mid -> end is in order and the target is in that range
        // look at right side, otherwise look at left side.
        if ((nums(mid) to nums(end)).contains(target))
          go(mid + 1, end)
        else
          go(start, mid - 1)
      }
    }
    go(0, nums.length - 1)
  }

  def checkRotated(nums: Array[Int]): Boolean = {
    @tailrec def go(i: Int, pivotReached: Boolean): Boolean =
      if (i >= nums.length)
        true
      else if (nums(i) > nums(i - 1))
        go(i + 1, pivotReached)
      else if (!pivotReached && nums(i) != nums(i - 1))
        go(i + 1, true)
      else
        false
    if (nums.length < 2) true else go(1, false)
  }

  def promptRotated: IO[Array[Int]] =
    for {
      _         <- IO(println("Enter a sorted array of integers without duplicates separated by spaces"))
      nums      <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt))
      validated <- if (checkRotated(nums)) IO.pure(nums) else promptRotated
    } yield validated

  def run: IO[Unit] =
    for {
      nums   <- promptRotated
      _      <- IO(println("Enter target to search for:"))
      target <- IO(StdIn.readInt)
      _      <- IO(println("Result index is: " + search(nums, target)))
    } yield ()
}
