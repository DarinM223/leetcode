package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object MostWater {
  def maxArea(height: Array[Int]): Int = {
    def search(i: Int, until: Int, dir: Int): Option[Int] = {
      def go(j: Int): Option[Int] =
        if ((j >= until && dir > 0) || (j <= until && dir < 0))
          None
        else if (height(j) > height(i))
          Some(j)
        else
          go(j + dir)
      go(i)
    }
    def volume(start: Int, end: Int): Int =
      height(start).min(height(end)) * (end - start)
    // (start, end) ranges start from the ends and go inward
    // only pick candidates more inward that are bigger than it
    @tailrec def go(start: Int, end: Int, maxStart: Int, maxEnd: Int): Int = {
      if (height(start) < height(end)) {
        // start has to find a bigger candidate
        search(start, end, 1) match {
          case Some(newStart)
            if volume(newStart, end) > volume(maxStart, maxEnd) =>
            go(newStart, end, newStart, end)
          case Some(newStart) =>
            go(newStart, end, maxStart, maxEnd)
          case None =>
            volume(maxStart, maxEnd)
        }
      } else if (height(end) < height(start)) {
        // end has to find a bigger candidate
        search(end, start, -1) match {
          case Some(newEnd)
            if volume(start, newEnd) > volume(maxStart, maxEnd) =>
            go(start, newEnd, start, newEnd)
          case Some(newEnd) =>
            go(start, newEnd, maxStart, maxEnd)
          case None =>
            volume(maxStart, maxEnd)
        }
      } else {
        // both start and end have to find a bigger candidate
        (search(start, end, 1), search(end, start, -1)) match {
          case (Some(newStart), Some(newEnd))
            if volume(newStart, newEnd) > volume(maxStart, maxEnd) =>
            go(newStart, newEnd, newStart, newEnd)
          case (Some(newStart), Some(newEnd)) =>
            go(newStart, newEnd, maxStart, maxEnd)
          case _ =>
            volume(maxStart, maxEnd)
        }
      }
    }
    go(0, height.length - 1, 0, height.length - 1)
  }

  // Doesn't skip unnecessary volume calculations but less code and cleaner.
  // On every iteration it updates the max volume and moves the index
  // with the smaller height inward. If both have the same height it doesn't matter
  // which moves first because both need to find bigger indexes to update max anyway.
  def maxAreaCleaner(height: Array[Int]): Int = {
    def volume(start: Int, end: Int): Int =
      height(start).min(height(end)) * (end - start)
    @tailrec def go(start: Int, end: Int, maxStart: Int, maxEnd: Int): Int = {
      lazy val (newMaxStart, newMaxEnd) =
        if (volume(start, end) > volume(maxStart, maxEnd))
          (start, end)
        else
          (maxStart, maxEnd)
      if (end <= start)
        volume(maxStart, maxEnd)
      else if (height(start) < height(end))
        go(start + 1, end, newMaxStart, newMaxEnd)
      else
        go(start, end - 1, newMaxStart, newMaxEnd)
    }
    go(0, height.length - 1, 0, height.length - 1)
  }

  def run: IO[Unit] =
    for {
      _      <- IO(println("Enter the array of integers representing the heights separated by spaces"))
      height <- IO(StdIn.readLine).map(_.split(" ").map(_.toInt))
      _      <- IO(println("Largest volume: " + maxAreaCleaner(height)))
    } yield ()
}
