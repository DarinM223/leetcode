package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object LongestPalindrome {
  // Brute force: iterate through all ranges (two integers for range)
  // and check if palindrome. O(n^3)
  def longestPalindromeBruteForce(s: String): String = {
    def isPalindrome(s: String): Boolean = {
      @tailrec def go(s: String, i: Int): Boolean = {
        val (begin, end) = (i, s.length - 1 - i)
        if (begin == end)          true
        else if (end == begin + 1) s(begin) == s(end)
        else                       s(begin) == s(end) && go(s, i + 1)
      }
      if (s.isEmpty) true else go(s, 0)
    }
    val ranges =
      for {
        start <- Seq.range(0, s.length)
        end   <- Seq.range(start, s.length)
      } yield (start, end)
    ranges.foldLeft("") { case (max, (start, end)) =>
      val substr = s.substring(start, end + 1)
      if (isPalindrome(substr) && substr.length > max.length)
        substr
      else
        max
    }
  }

  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy(expr)
  }

  // Use dynamic programming to reuse work. O(n^2)
  def longestPalindromeDP(s: String): String = {
    def isPalindrome(begin: Int, end: Int): Lazy[Boolean] = Lazy {
      if (begin == end) {
        true
      } else if (end == begin + 1) {
        s(begin) == s(end)
      } else {
        s(begin) == s(end) && {
          if ((end - 1) > (begin + 1)) table(begin + 1)(end - 1)() else true
        }
      }
    }
    lazy val table = Array
      .tabulate[Lazy[Boolean]](s.length, s.length)(isPalindrome)
    val ranges =
      for {
        start <- Seq.range(0, s.length)
        end   <- Seq.range(start, s.length)
      } yield (start, end)
    ranges.foldLeft("") { case (max, (start, end)) =>
      lazy val substr = s.substring(start, end + 1)
      if (table(start)(end)() && substr.length > max.length)
        substr
      else
        max
    }
  }

  // Iterates over palindrome centers and expands around the center. O(n^2)
  def longestPalindromeExpand(s: String): String = s match {
    case "" => ""
    case _ =>
      @tailrec def expandAroundCenter(s: String, left: Int, right: Int): Int =
        if (left >= 0 && right < s.length && s(left) == s(right))
          expandAroundCenter(s, left - 1, right + 1)
        else
          right - left - 1
      val (start, end) = (0 until s.length).foldLeft((0, 0)) {
        case ((start, end), i) =>
          val len1 = expandAroundCenter(s, i, i)
          val len2 = expandAroundCenter(s, i, i + 1)
          val len = len1.max(len2)
          if (len > end - start)
            (i - (len - 1) / 2, i + len / 2)
          else
            (start, end)
      }
      s.substring(start, end + 1)
  }

  val approaches: Map[String, String => String] = Map(
    "brute force"         -> longestPalindromeBruteForce,
    "dynamic programming" -> longestPalindromeDP,
    "expand from center"  -> longestPalindromeExpand,
  )

  def run: IO[Unit] =
    for {
      fn <- IOUtils.promptApproach(approaches)
      _  <- IO(println("Enter the string to find the longest palindrome in:"))
      s  <- IO(StdIn.readLine())
      _  <- IO(println("Result is: " + fn(s)))
    } yield ()
}
