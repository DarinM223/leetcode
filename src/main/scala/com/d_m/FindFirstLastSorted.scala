package com.d_m

import cats.effect.IO

import scala.io.StdIn

object FindFirstLastSorted {
  def searchRange(nums: Array[Int], target: Int): Array[Int] = {
    def go(start: Int, end: Int): Array[Int] = {
      val mid = (start + end) / 2
      if (end < start) {
        Array(-1, -1)
      } else if (nums(mid) == target) {
        lazy val recLeft = go(start, mid - 1)
        lazy val recRight = go(mid + 1, end)
        // Recurse on sides if there are more duplicate values equal to target
        // to the left or right of the midpoint index.
        (nums.lift(mid - 1), nums.lift(mid + 1)) match {
          case (Some(i), Some(j)) if i == target && j == target =>
            Array(recLeft(0), recRight(1))
          case (Some(i), _) if i == target =>
            Array(recLeft(0), mid)
          case (_, Some(i)) if i == target =>
            Array(mid, recRight(1))
          case _ => Array(mid, mid)
        }
      } else if (nums(mid) < target) {
        go(mid + 1, end)
      } else {
        go(start, mid - 1)
      }
    }
    go(0, nums.length - 1)
  }

  def run: IO[Unit] =
    for {
      _      <- IO(println("Enter a sorted array of integers separated by spaces:"))
      nums   <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt).sorted)
      _      <- IO(println("Sorted array is: " + nums.mkString(" ")))
      _      <- IO(println("Enter the target to search for:"))
      target <- IO(StdIn.readInt)
      _      <- IO(println("Result range is: [" + searchRange(nums, target).mkString(",") + "]"))
    } yield ()
}
