package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object MedianTwoSorted {
  def findMedianSortedArrays(nums1: Array[Int], nums2: Array[Int]): Double = {
    // What is a median used for?
    // Dividing a set into two equal length subsets,
    // that one subset is always greater than the other.

    // Want to divide merged A and B
    // Accomplish this by dividing both A and B into two pieces
    // by different indexes i and j.
    //
    // Then:
    //         left_part         |           right_part
    // A[0], A[1], ..., A[i - 1] | A[i], A[i + 1], ..., A[m - 1]
    // B[0], B[1], ..., B[j - 1] | B[j], B[j + 1], ..., B[n - 1]
    //
    // To ensure median, max(left_part) <= min(right_part).
    // So everything on the left part is less than or equal to
    // everything on the right part.
    //
    // We already know that A[i - 1] <= A[i] and B[j - 1] <= B[j].
    // So we want i and j so that A[i - 1] <= B[j] and B[j - 1] <= A[i].
    //
    // We also want length(left_part) == length(right_part).
    // That means i + j = (m - i) + (n - j) + 1
    // ~> j = (m + n + 1) / 2 - i

    // Make sure n >= m where n = length(a) and m = length(b)
    // so that j doesn't become negative (bc 0 <= i <= m).
    val (a, b, m, n) =
      if (nums1.length > nums2.length) {
        (nums2, nums1, nums2.length, nums1.length)
      } else {
        (nums1, nums2, nums1.length, nums2.length)
      }
    val halfLen = (m + n + 1) / 2
    @tailrec def go(imin: Int, imax: Int): Double = {
      val i = (imin + imax) / 2
      val j = halfLen - i
      if (i < m && b(j - 1) > a(i)) {
        // i is too small, must increase it
        go(i + 1, imax)
      } else if (i > 0 && a(i - 1) > b(j)) {
        // i is too big, must decrease it
        go(imin, i - 1)
      } else {
        lazy val maxLeftPart =
          if (i == 0)      b(j - 1)
          else if (j == 0) a(i - 1)
          else             a(i - 1).max(b(j - 1))
        lazy val minRightPart =
          if (i == m)      b(j)
          else if (j == n) a(i)
          else             a(i).min(b(j))
        if ((m + n) % 2 == 1) {
          maxLeftPart
        } else {
          (maxLeftPart + minRightPart).toDouble / 2.0
        }
      }
    }
    go(0, m)
  }

  def run: IO[Unit] =
    for {
      _     <- IO(println("Enter the first array (separated by spaces)"))
      nums1 <- IO(StdIn.readLine()).map(_.split(' ').map(_.toInt))
      _     <- IO(println("Enter the second array (separated by spaces)"))
      nums2 <- IO(StdIn.readLine()).map(_.split(' ').map(_.toInt))
      _     <- IO(println("Result: " + findMedianSortedArrays(nums1, nums2)))
    } yield ()
}
