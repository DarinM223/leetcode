package com.d_m

import cats.effect.IO

import scala.io.StdIn

object SpiralMatrix {
  def spiralOrder(matrix: Array[Array[Int]]): List[Int] = {
    def changeDirection(direction: (Int, Int)): (Int, Int) = direction match {
      case (0, 1) => (1, 0)   // Left-to-right to top-to-bottom
      case (1, 0) => (0, -1)  // Top-to-bottom to right-to-left
      case (0, -1) => (-1, 0) // Right-to-left to bottom-to-top
      case (-1, 0) => (0, 1)  // Bottom-to-top to left-to-right
      case _ => ???
    }
    def outOfBounds(dr: Int, dc: Int, pr: Int, pc: Int, visited: Set[(Int, Int)]): Boolean =
      pr + dr < 0 || pr + dr >= matrix.length ||
      pc + dc < 0 || pc + dc >= matrix(0).length ||
      visited.contains((pr + dr, pc + dc))

    def go(direction: (Int, Int), point: (Int, Int), visited: Set[(Int, Int)]): List[Int] =
      (direction, point) match {
        case ((dr, dc), (pr, pc)) =>
          if (outOfBounds(dr, dc, pr, pc, visited)) {
            val (ndr, ndc) = changeDirection(direction)
            if (outOfBounds(ndr, ndc, pr, pc, visited))
              List(matrix(pr)(pc))
            else
              matrix(pr)(pc) :: go((ndr, ndc), (ndr + pr, ndc + pc), visited + point)
          } else {
            matrix(pr)(pc) :: go((dr, dc), (dr + pr, dc + pc), visited + point)
          }
      }
    if (matrix.isEmpty || matrix(0).isEmpty)
      List()
    else
      go((0, 1), (0, 0), Set())
  }

  def promptGrid: IO[Array[Array[Int]]] = {
    def go: IO[List[List[Int]]] =
      for {
        l <- IO(StdIn.readLine)
        r <- if (l.isEmpty)
          IO.pure(Nil)
        else
          go.map(l.split(' ').map(_.toInt).toList :: _)
      } yield r
    go.map(_.map(_.toArray).toArray)
  }

  def run: IO[Unit] =
    for {
      matrix <- IOUtils.promptGrid
      _      <- IO(println("Spiral order is: " + spiralOrder(matrix).mkString(" ")))
    } yield ()
}
