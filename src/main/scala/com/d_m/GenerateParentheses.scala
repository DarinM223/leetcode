package com.d_m

import cats.effect.IO

import scala.io.StdIn

object GenerateParentheses {
  def generateParentheses(n: Int): List[String] = {
    def go(build: List[String], s: List[Char], n: Int, unclosedParens: Int): List[String] =
      if (n == 0 && unclosedParens == 0) {
        s.mkString :: build
      } else {
        // Can add a new open parenthesis ')' if n > 0
        // Can add a new close parenthesis '(' if unclosedParens > 0
        if (n > 0 && unclosedParens > 0) {
          val build2 = go(build, '(' :: s, n, unclosedParens - 1)
          go(build2, ')' :: s, n - 1, unclosedParens + 1)
        } else if (unclosedParens > 0) {
          go(build, '(' :: s, n, unclosedParens - 1)
        } else {
          go(build, ')' :: s, n - 1, unclosedParens + 1)
        }
      }
    go(List(), List(), n, 0)
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter the number of parentheses pairs to generate:"))
      n <- IO(StdIn.readInt)
      _ <- IO(println("Parentheses combinations: " + generateParentheses(n).mkString(" ")))
    } yield ()
}
