package com.d_m

import cats.effect.IO

import scala.io.StdIn

object LongestSubstring {
  def lengthOfLongestSubstring(s: String): Int = {
    s.zipWithIndex.foldLeft((Map[Char, Int](), 0, 0, 0)) {
      case ((dict, start, curr, max), (c, j)) =>
        val newDict = dict + (c -> j)
        dict.get(c) match {
          case Some(i) =>
            val newStart = start.max(i)
            val newCurr = j - newStart
            (newDict, newStart, newCurr, newCurr.max(max))
          case None =>
            val newCurr = curr + 1
            (newDict, start, newCurr, newCurr.max(max))
        }
    }._4
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter string:"))
      s <- IO(StdIn.readLine())
      _ <- IO(println("Result: " + lengthOfLongestSubstring(s)))
    } yield ()
}
