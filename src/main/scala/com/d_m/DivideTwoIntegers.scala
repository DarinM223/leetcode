package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object DivideTwoIntegers {
  // Handle MinValue / -1 case and convert to long first to handle overflow.
  def divide(dividend: Int, divisor: Int): Int =
    if (dividend == Int.MinValue && divisor == -1) {
      Int.MaxValue
    } else {
      val absDivisor: Long = divisor.toLong.abs
      @tailrec def go(dividend: Long, acc: Long): Long =
        if (dividend.abs < absDivisor) {
          acc
        } else {
          val updatedDividend =
            if (dividend < 0) dividend + absDivisor
            else              dividend - absDivisor
          go(updatedDividend, acc + 1)
        }
      val result = go(dividend.toLong, 0)
      if ((dividend < 0 && divisor >= 0) || (dividend >= 0 && divisor < 0))
        -result.toInt
      else
        result.toInt
    }

  def run: IO[Unit] =
    for {
      _        <- IO(println("Enter dividend:"))
      dividend <- IO(StdIn.readInt)
      _        <- IO(println("Enter divisor:"))
      divisor  <- IO(StdIn.readInt)
      _        <- IO(println("Result: " + divide(dividend, divisor)))
    } yield ()
}
