package com.d_m

import cats.effect.IO
import cats.implicits._

import scala.io.StdIn

object IOUtils {
  def promptApproach[F](approaches: Map[String, F]): IO[F] =
    for {
      _        <- IO(println("Enter the approach:"))
      approach <- IO(StdIn.readLine())
      fn       <- approaches.get(approach) match {
        case Some(f) => IO.pure(f)
        case None =>
          val err = "Available approaches: " + approaches.keys.mkString(", ")
          IO(println(err)) >> promptApproach(approaches)
      }
    } yield fn

  def promptGrid: IO[Array[Array[Int]]] = {
    def go: IO[List[List[Int]]] =
      for {
        l <- IO(StdIn.readLine)
        r <- if (l.isEmpty)
          IO.pure(Nil)
        else
          go.map(l.split(' ').map(_.toInt).toList :: _)
      } yield r
    IO(println("Enter a 2d matrix separated by spaces and newlines and enter another newline when done:")) >>
      go.map(_.map(_.toArray).toArray)
  }
}
