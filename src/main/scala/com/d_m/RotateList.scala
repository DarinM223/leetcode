package com.d_m

import cats.effect.IO

import scala.annotation.tailrec
import scala.io.StdIn

object RotateList {
  import ListNode._

  def rotateRight(head: List[Int], k: Int): List[Int] = {
    @tailrec def stepBack(array: Array[Int], i: Int, k: Int): Int =
      if (k == 0)
        i
      else if (i == 0)
        stepBack(array, array.length - 1, k - 1)
      else
        stepBack(array, i - 1, k - 1)
    def stepForward(array: Array[Int], i: Int, l: Int): List[Int] =
      if (l == 0)
        Nil
      else if (i == array.length - 1)
        array(i) :: stepForward(array, 0, l - 1)
      else
        array(i) :: stepForward(array, i + 1, l - 1)
    val array = head.toArray
    val start = stepBack(array, 0, k)
    stepForward(array, start, array.length)
  }

  def rotateRight(head: ListNode, k: Int): ListNode =
    fromList(rotateRight(toList(head), k))

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter a list of integers separated by spaces:"))
      l <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt).toList)
      _ <- IO(println("Enter the amount to rotate the list:"))
      k <- IO(StdIn.readInt)
      _ <- IO(println("Result: " + rotateRight(l, k).mkString(" ")))
    } yield ()
}