package com.d_m
import cats.effect.IO
import scala.io.StdIn

object UniquePaths {
  def uniquePaths(m: Int, n: Int): Int = {
    def go(row: Int, col: Int): Int =
      if (row >= n || col >= m)
        0
      else if (row == n - 1 && col == m - 1)
        1
      else
        go(row + 1, col) + go(row, col + 1)
    go(0, 0)
  }

  class Lazy[T](expr: => T) {
    lazy val value = expr
    def apply(): T = this.value
  }
  object Lazy {
    def apply[T](expr: => T) = new Lazy[T](expr)
  }

  def uniquePathsDP(m: Int, n: Int): Int = {
    def build(row: Int, col: Int): Lazy[Int] = Lazy {
      if (row >= n || col >= m)
        0
      else if (row == n - 1 && col == m - 1)
        1
      else
        table(row + 1)(col)() + table(row)(col + 1)()
    }
    lazy val table = Array.tabulate(n + 1, m + 1)(build)
    table(0)(0)()
  }

  // Can think of number of permutations of a string with D for down and R for right.
  // Example: for a 3x7 matrix one path is:
  // D R R R D R R R
  // The number of unique paths is the number of permutations of this string.
  // Total permutations = (m' + n')! / (m'! * n'!)
  // where m' = m - 1 and n' = n - 1 and m' > n'
  // Avoids factorial by repeatedly multiplying and dividing every step.
  def uniquePathsMath(m: Int, n: Int): Int = {
    val m2 = m.max(n) - 1
    val n2 = m.min(n) - 1
    (m2 + 1 to m2 + n2).foldLeft((1: Long, 1)) {
      case ((res, j), i) => ((res * i) / j, j + 1)
    }._1.toInt
  }

  val approaches: Map[String, (Int, Int) => Int] = Map(
    "recursive"           -> uniquePaths,
    "dynamic programming" -> uniquePathsDP,
    "math"                -> uniquePathsMath,
  )

  def run: IO[Unit] =
    for {
      fn <- IOUtils.promptApproach(approaches)
      _  <- IO(println("Enter integer m (in m x n grid):"))
      m  <- IO(StdIn.readInt)
      _  <- IO(println("Enter integer n (in m x n grid):"))
      n  <- IO(StdIn.readInt)
      _  <- IO(println("# of unique paths: " + fn(m, n)))
    } yield ()
}