package com.d_m

import cats.effect.IO

import scala.io.StdIn

object SwapNodesInPairs {
  def swapPairs(head: ListNode): ListNode = {
    import ListNode._
    fromList(swapPairs(toList(head)))
  }

  def swapPairs(l: List[Int]): List[Int] = l match {
    case h1 :: h2 :: rest => h2 :: h1 :: swapPairs(rest)
    case l => l
  }

  def run: IO[Unit] =
    for {
      _ <- IO(println("Enter list of numbers separated by spaces:"))
      l <- IO(StdIn.readLine).map(_.split(' ').map(_.toInt).toList)
      _ <- IO(println("Result: " + swapPairs(l).mkString(" ")))
    } yield ()
}
